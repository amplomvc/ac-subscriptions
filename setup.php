<?php

/**
 * Subscription Integration with Products
 *
 * Version: 0.5.1
 * Name: subscription
 * Title: Subscriptions
 * Description: Allows customers to purchase subscriptions to anything you would like them to subscribe to. Uses the products as a subscription object.
 * Author: Daniel Newman
 * Date: 3/15/2013
 * Dependencies: amplocart
 * Link: http://www.amplocart.com/plugins/subscription
 *
 */
class Plugin_Subscription_Setup extends Plugin_Setup
{
	static $admin_links = array(
		'cart_subscription' => array(
			'display_name' => "Subscriptions",
			'path'         => 'admin/subscription',
			'parent'       => 'cart',
			'sort_order'   => 20,
		),
		'customer_subscription' => array(
			'display_name' => "Customer Subscriptions",
			'path'         => 'admin/customer/subscription',
			'parent'       => 'customer',
			'sort_order'   => 2,
		),
	);

	public function install()
	{
		require_once(DIR_PLUGIN . 'subscription/install.php');

		//Add Admin Nav Links
		$this->Model_Navigation->saveGroupLinks('admin', self::$admin_links);
	}

	public function upgrade($from_version)
	{
		if (version_compare($from_version, '0.5.1', '<')) {
			$this->db->addColumn('customer_subscription', 'subscription_id', "INT(10) UNSIGNED NOT NULL AFTER `product_id`");
		}
	}

	public function uninstall($keep_data = false)
	{
		if (!$keep_data) {
			$this->db->dropTable('subscription');
			$this->db->dropTable('product_subscription');
			$this->db->dropTable('customer_subscription');
			$this->db->dropTable('customer_subscription_option');
			$this->db->dropTable('customer_subscription_history');
			$this->db->dropTable('customer_subscription_meta');

			$this->db->dropColumn('customer_transaction', 'subscription_id');
			$this->db->dropColumn('shipping', 'subscription_id');
		}

		$this->Model_Navigation->removeGroupLinks('admin', self::$admin_links);
	}
}
