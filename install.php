<?php
$this->db->createTable('subscription', <<<SQL
		  `subscription_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
		  `code` varchar(45) NOT NULL,
		  `name` varchar(255) NOT NULL,
		  `description` text,
		  `value_function` varchar(100) DEFAULT NULL,
		  `recurring` tinyint(1) unsigned NOT NULL DEFAULT '1',
		  `cycles` int(11) unsigned NOT NULL DEFAULT '0',
		  `time` int(11) unsigned NOT NULL DEFAULT '1',
		  `time_unit` varchar(2) NOT NULL DEFAULT 'M',
		  `day` int(10) unsigned NOT NULL,
		  `shipping_recurring` tinyint(1) unsigned NOT NULL,
		  `shipping_cycles` int(10) unsigned NOT NULL,
		  `shipping_time` int(10) unsigned NOT NULL,
		  `shipping_time_unit` varchar(2) NOT NULL,
		  `shipping_day` int(10) unsigned NOT NULL,
		  `retries` int(10) unsigned NOT NULL DEFAULT '0',
		  `status` tinyint(4) NOT NULL DEFAULT '1',
		  `date_added` datetime NOT NULL,
		  PRIMARY KEY (`subscription_id`)
SQL
);

$this->db->createTable('product_subscription', <<<SQL
			`product_id` int(10) unsigned NOT NULL,
			`subscription_id` int(10) unsigned NOT NULL,
			`sort_order` int(11) DEFAULT NULL,
			PRIMARY KEY (`subscription_id`,`product_id`)
SQL
);

/*
 * Notice the lack of a subscription ID. This is because we use the product which is associated to 1 or more subscriptions
 * This is how we determine the current payment rate in a progressive subscription policy. We also use the Product to calculate
 * subscription options, etc..
 */
$this->db->createTable('customer_subscription', <<<SQL
		  `customer_subscription_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
		  `customer_id` int(10) unsigned NOT NULL,
		  `product_id` int(10) unsigned NOT NULL,
		  `subscription_id` int(10) unsigned NOT NULL,
		  `payment_profile_id` int(10) unsigned NOT NULL,
		  `shipping_address_id` int(10) unsigned NOT NULL,
		  `shipping_code` varchar(128) NOT NULL,
		  `shipping_key` varchar(255) NOT NULL,
		  `status` int unsigned NOT NULL,
		  `date_added` datetime NOT NULL,
		  `date_activated` datetime NOT NULL,
		  PRIMARY KEY (`customer_subscription_id`)
SQL
);


$this->db->createTable('customer_subscription_option', <<<SQL
		  `customer_subscription_option_id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
		  `customer_subscription_id` INT UNSIGNED NOT NULL ,
		  `product_id` INT UNSIGNED NOT NULL ,
		  `product_option_id` INT UNSIGNED NOT NULL ,
		  `product_option_value_id` INT UNSIGNED NOT NULL ,
		  PRIMARY KEY (`customer_subscription_option_id`)
SQL
);

$this->db->createTable('customer_subscription_history', <<<SQL
		  `customer_subscription_history_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
		  `customer_subscription_id` int(10) unsigned NOT NULL,
		  `type` varchar(45) NOT NULL,
		  `transaction_id` int(10) unsigned NOT NULL,
		  `shipping_id` int(10) unsigned NOT NULL,
		  `data` text,
		  `status` varchar(45) NOT NULL,
		  `comment` text,
		  `date_added` datetime NOT NULL,
		  PRIMARY KEY (`customer_subscription_history_id`)
SQL
);

$this->db->createTable('customer_subscription_meta', <<<SQL
		  `customer_subscription_meta_id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
		  `customer_subscription_id` INT UNSIGNED NOT NULL ,
		  `key` VARCHAR(45) NOT NULL ,
		  `value` TEXT NOT NULL ,
		  `serialized` TINYINT UNSIGNED NOT NULL ,
		  PRIMARY KEY (`customer_subscription_meta_id`)
SQL
);
