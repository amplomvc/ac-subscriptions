<?php
class System_Cron_Subscription extends System_Cron_Job
{
	public function automate()
	{
		$this->billSubscriptions();

		$this->shipSubscriptions();

		$this->resumeSubscriptions();
	}

	public function billSubscriptions()
	{
		$this->subscription->chargeSubscriptions();

		//Clear Errors in case Cron was called from browser
		$this->subscription->clearErrors();

		$this->subscription->cancelDelinquentSubscriptions();
	}

	public function shipSubscriptions()
	{
		$this->subscription->shipSubscriptions();

		//Clear Errors in case Cron was called from browser
		$this->subscription->clearErrors();
	}

	public function resumeSubscriptions()
	{
		$now                       = $this->date->now();
		$customer_subscription_ids = $this->db->queryColumn("SELECT customer_subscription_id FROM {$this->t['customer_subscription_meta']} WHERE `key` = 'resume_date' AND DATE(value) <= DATE('$now')");

		foreach ($customer_subscription_ids as $customer_subscription_id) {
			$this->subscription->resumeCustomerSubscription($customer_subscription_id);
		}
	}
}
