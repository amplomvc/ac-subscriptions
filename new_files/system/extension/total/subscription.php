<?php

class System_Extension_Total_Subscription extends System_Extension_Total
{
	public function getTotal(&$total_data, &$total, &$taxes)
	{
		$subscription_total = 0;

		$subscriptions = array();

		$cart_subscriptions = $this->subscription->getCartSubscriptions();

		foreach ($cart_subscriptions as $cart_subscription) {
			$subscription_total += $cart_subscription['total'];
			$subscription_id = $cart_subscription['subscription']['subscription_id'];

			if (!isset($subscriptions[$subscription_id])) {
				$subscriptions[$subscription_id] = array(
					'total'        => $cart_subscription['total'],
					'subscription' => $cart_subscription['subscription'],
				);
			} else {
				$subscriptions[$subscription_id]['total'] += $cart_subscription['total'];
			}
		}


		//Do not show Subscription totals during checkout.
		//Also hide subscription totals when there are no subscriptions
		if ($this->route->getSegment(0) === 'checkout' || empty($subscriptions)) {
			return false;
		}

		$text = '';

		foreach ($subscriptions as $subscription) {
			$text .= ($text ? '<br/>+' : '') . $this->subscription->formatPrice($subscription['subscription'], $subscription['total']);
		}

		$total_data['subscription'] = array(
			'amount'     => 0,
			'text'       => $text,
			'sort_order' => 20,
		) + $this->info();
	}
}
