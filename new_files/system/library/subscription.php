<?php

class Subscription extends Library
{
	//Cart products
	const CART_SUBSCRIPTIONS = 'subscriptions';

	//Statuses
	const
		STATUS_PENDING = 1,
		STATUS_ACTIVE = 2,
		STATUS_EXPIRED = 3,
		STATUS_CANCELLED = 4,
		STATUS_PAUSED = 5;

	static $statuses = array(
		self::STATUS_PENDING   => 'Pending',
		self::STATUS_ACTIVE    => 'Active',
		self::STATUS_EXPIRED   => 'Expired',
		self::STATUS_CANCELLED => 'Cancelled',
		self::STATUS_PAUSED    => 'Paused',
	);

	//Time Units
	public static $time_units = array(
		'D' => "Day",
		'W' => "Week",
		'M' => "Month",
		'Y' => "Year",
	);

	private $subscriptions = array();

	/******************
	 * Cart Functions *
	 ******************/

	public function cartEmpty()
	{
		return !$this->cart->has(self::CART_SUBSCRIPTIONS);
	}

	public function getCart()
	{
		return $this->cart->get(self::CART_SUBSCRIPTIONS);
	}

	public function getCartSubscription($key)
	{
		$subscription = $this->cart->getItem(self::CART_SUBSCRIPTIONS, $key);

		if ($subscription) {
			if (!$this->fillSubscriptionDetails($subscription)) {
				$this->removeFromCart($key);
			}

			return $subscription;
		}

		return null;
	}

	public function getCartSubscriptions()
	{
		$subscriptions = $this->getCart();

		foreach ($subscriptions as $key => &$subscription) {
			if (!$this->fillSubscriptionDetails($subscription, $subscription['options'])) {
				$this->removeFromCart($key);
			}
		}
		unset($subscription);

		return $subscriptions;
	}

	public function addToCart($product_id, $options)
	{
		return $this->cart->addItem(self::CART_SUBSCRIPTIONS, $product_id, 1, $options);
	}

	public function updateCart($key, $quantity)
	{
		$this->cart->updateItem(self::CART_SUBSCRIPTIONS, $key, $quantity);
	}

	public function removeFromCart($key)
	{
		return $this->cart->removeItem(self::CART_SUBSCRIPTIONS, $key);
	}

	/***********************************
	 * Customer Subscription Functions *
	 ***********************************/

	/**
	 * Adds a customer subscription to the database.
	 * The initial state is Pending. Must Call subscription->activateCustomerSubscription() method to activate the subscription.
	 *
	 * @param array $customer_subscription - The customer subscription information
	 * @return the newly created Customer Subscription ID
	 */

	public function addCustomerSubscription($customer_subscription)
	{
		if (!$this->customer->isLogged()) {
			$this->error['customer'] = _("Customer must be logged in to add a Customer Subscription.");
			return false;
		}

		$customer_subscription['customer_id'] = customer_info('customer_id');
		$customer_subscription['status']      = self::STATUS_PENDING;

		$customer_subscription_id = $this->insert('customer_subscription', $customer_subscription);

		if (!empty($customer_subscription['options'])) {
			$this->updateCustomerSubscriptionOptions($customer_subscription_id, $customer_subscription['product_id'], $customer_subscription['options']);
		}

		//Add Subscription history
		$history_data = array(
			'status'  => self::STATUS_PENDING,
			'comment' => _l("Added Customer Subscription"),
		);

		$this->addHistory($customer_subscription_id, 'add', $history_data);

		return $customer_subscription_id;
	}

	public function activateCustomerSubscription($customer_subscription_id)
	{
		if ($this->updateCustomerSubscriptionStatus($customer_subscription_id, self::STATUS_ACTIVE)) {
			$this->update('customer_subscription', array('date_activated' => $this->date->now()), $customer_subscription_id);

			return true;
		}

		return false;
	}

	public function updateCustomerSubscription($customer_subscription_id, $data)
	{
		if (!$this->verifyCustomerSubscription($customer_subscription_id)) {
			return false;
		}

		$locked = array(
			'customer_id',
			'product_id',
			'date_added',
			'date_activated',
			'status',
		);

		$update_status = false;

		foreach ($data as $key => $value) {
			if (in_array($key, $locked)) {
				unset($data[$key]);
			}

			if ($key === 'status') {
				$update_status = $value;
			}
		}

		//If there are values to update besides the locked values
		if (!empty($data)) {
			//Save historical subscription data
			$customer_subscription            = $this->Model_Customer_Subscription->getRecord($customer_subscription_id);
			$customer_subscription['options'] = $this->getCustomerSubscriptionOptions($customer_subscription_id);

			if (isset($data['shipping_address'])) {
				if (empty($data['shipping_address']['address_id'])) {
					$address_id = $this->Model_Customer->saveAddress(customer_info('customer_id'), null, $data['shipping_address']);

					if (!$address_id) {
						$this->error['shipping_address'] = $this->Model_Customer->getError();
						return false;
					}

					$data['shipping_address_id'] = $address_id;
				} else {
					$data['shipping_address_id'] = $data['shipping_address']['address_id'];
				}
			}

			if (isset($data['payment'])) {
				if (empty($data['payment']['profile_id'])) {
					$profile_id = $this->payment->createProfile($data['payment']);

					if (!$profile_id) {
						$this->error = $this->payment->getError();
						return false;
					}

					$data['payment_profile_id'] = $profile_id;
				} else {
					$data['payment_profile_id'] = $data['payment']['profile_id'];
				}
			}

			if (!isset($data['options']) || count($data) > 1) {
				if (!$this->update('customer_subscription', $data, $customer_subscription_id)) {
					return false;
				}
			}

			$history_data = array(
				'comment' => _l("Subscription Updated"),
				'status'  => $customer_subscription['status'],
				'data'    => serialize($customer_subscription),
			);

			$this->addHistory($customer_subscription_id, 'update', $history_data);

			if (!empty($data['options'])) {
				$this->updateCustomerSubscriptionOptions($customer_subscription_id, $customer_subscription['product_id'], $data['options']);
			}
		}

		//Always update the status in a separate transaction
		if ($update_status) {
			$this->updateCustomerSubscriptionStatus($customer_subscription_id, $update_status);
		}

		//Invalidate the customer subscription
		$this->invalidate($customer_subscription_id);

		return $customer_subscription_id;
	}

	public function updateCustomerSubscriptionOptions($customer_subscription_id, $product_id, $options)
	{
		$this->delete('customer_subscription_option', array('customer_subscription_id' => $customer_subscription_id));

		foreach ($options as $option_id => $option_values) {

			if (!is_array($option_values)) {
				$option_values = array($option_values);
			}

			foreach ($option_values as $option_value_id) {
				$customer_subscription_option = array(
					'customer_subscription_id' => $customer_subscription_id,
					'product_id'               => $product_id,
					'product_option_id'        => $option_id,
					'product_option_value_id'  => is_array($option_value_id) ? $option_value_id['product_option_value_id'] : $option_value_id,
				);

				$this->insert('customer_subscription_option', $customer_subscription_option);
			}
		}
	}

	public function updateCustomerSubscriptionStatus($customer_subscription_id, $status)
	{
		$customer_subscription = $this->getCustomerSubscription($customer_subscription_id);

		if (!$customer_subscription) {
			return false;
		}

		if ($customer_subscription['status'] === $status) {
			$this->error['status'] = _l("Customer Subscription status is unchanged ($status)");
			return false;
		}

		$this->update('customer_subscription', array('status' => $status), $customer_subscription_id);

		$history_data = array(
			'type'    => 'status update',
			'status'  => $status,
			'comment' => _l("Status Updated"),
		);

		$this->addHistory($customer_subscription_id, 'status_update', $history_data);

		$this->invalidate($customer_subscription_id);

		return true;
	}

	public function revertCustomerSubscription($customer_subscription_history_id)
	{
		$csh = $this->queryRow("SELECT * FROM {$this->t['customer_subscription_history']} WHERE customer_subscription_history_id = " . (int)$customer_subscription_history_id . " AND type = 'update'");

		$state = $csh ? unserialize($csh['data']) : false;

		if (!$state) {
			$this->error['history'] = _l("Customer Subscription historical state was not found!");
			return false;
		}

		//Unset key values
		unset($state['customer_subscription_id']);
		unset($state['status']);

		foreach ($state['options'] as &$option) {
			unset($option['customer_subscription_option_id']);
		}
		unset($option);

		//Add Historical record
		$history_data = array(
			'comment' => _l("Subscription Reverted"),
			'status'  => $csh['status'],
			'data'    => $customer_subscription_history_id,
		);

		$this->addHistory($csh['customer_subscription_id'], 'revert', $history_data);

		//Update the current subscription
		return $this->updateCustomerSubscription($csh['customer_subscription_id'], $state);
	}

	public function pauseCustomerSubscription($customer_subscription_id, $cycles)
	{
		if ($this->updateCustomerSubscriptionStatus($customer_subscription_id, self::STATUS_PAUSED)) {
			$customer_subscription = $this->getCustomerSubscription($customer_subscription_id);

			$subscription = $customer_subscription['subscription'];

			$resume_date = $this->date->now();

			while ($cycles-- >= 0) {
				$resume_date = $this->nextDueDate($customer_subscription['date_activated'], $resume_date, 0, $subscription['time'], $subscription['time_unit'], $subscription['day']);
			}

			$this->setMeta($customer_subscription_id, 'resume_date', $resume_date);
			$this->setMeta($customer_subscription_id, 'pause_date', $this->date->now());

			return $resume_date;
		}

		return false;
	}

	public function resumeCustomerSubscription($customer_subscription_id)
	{
		if ($this->updateCustomerSubscriptionStatus($customer_subscription_id, self::STATUS_ACTIVE)) {

			$this->deleteMeta($customer_subscription_id, 'resume_date');
			$this->deleteMeta($customer_subscription_id, 'pause_date');

			$customer_subscription = $this->getCustomerSubscription($customer_subscription_id);

			//Note: Customer messages are translated into the customers language when displayed
			$this->customer->sendMessage($customer_subscription['customer_id'], "Your Subscription to " . $customer_subscription['product']['name'] . " has been resumed!");

			return true;
		}

		return false;
	}

	public function cancelCustomerSubscription($customer_subscription_id)
	{
		return $this->updateCustomerSubscriptionStatus($customer_subscription_id, self::STATUS_CANCELLED);
	}

	public function removeCustomerSubscription($customer_subscription_id)
	{
		$subscription = $this->getCustomerSubscription($customer_subscription_id);

		if (!$subscription) {
			return false;
		}

		if ($subscription['status'] !== self::STATUS_CANCELLED) {
			$this->error = _l('You must cancel the subscription first before removing it.');
			return false;
		}

		$this->delete('customer_subscription', $customer_subscription_id);

		$where = array('customer_subscription_id' => $customer_subscription_id);

		$this->delete('customer_subscription_option', $where);
		$this->delete('customer_subscription_history', $where);
		$this->delete('customer_subscription_meta', $where);

		$this->invalidate($customer_subscription_id);

		return true;
	}

	public function getCustomerSubscription($customer_subscription_id)
	{
		if (!$this->verifyCustomerSubscription($customer_subscription_id)) {
			return false;
		}

		if (!isset($this->subscriptions[$customer_subscription_id])) {
			$subscription = cache("customer_subscription.$customer_subscription_id");

			if (empty($subscription['subscription'])) {
				$subscription = $this->queryRow("SELECT * FROM {$this->t['customer_subscription']} WHERE customer_subscription_id = " . (int)$customer_subscription_id);

				if ($subscription) {
					if (!$this->fillCustomerSubscriptionDetails($subscription)) {
						$url_contact                   = site_url('page', 'page_id=' . option('config_contact_page_id'));
						$subscription_name             = !empty($subscription['product']) ? $subscription['product']['name'] : _l("Unknown Subscription");
						$this->error['subscription'][] = _l("Your subscription %s is unavailable at this time. Please <a href=\"%s\">contact us</a> for more information.", $subscription_name, $url_contact);
						return null;
					}
				}

				if (empty($subscription['subscription'])) {
					$this->error['subscription'] = _l("The subscription you requested could not be found. Please try a different subscription.");
					return false;
				}

				cache("customer_subscription.$customer_subscription_id", $subscription);
			}

			//Add extra cycles if subscription was on hold.
			$subscription['hold_cycles'] = (int)$this->getMeta($customer_subscription_id, 'hold_cycles');
			$subscription['subscription']['cycles'] += $subscription['hold_cycles'];

			//Shipping hold Cycles
			$subscription['shipping_hold_cycles'] = (int)$this->getMeta($customer_subscription_id, 'shipping_hold_cycles');

			if (empty($subscription['subscription']['shipping_cycles'])) {
				$subscription['subscription']['shipping_cycles'] = 0;
			}

			$subscription['subscription']['shipping_cycles'] += $subscription['shipping_hold_cycles'];

			$this->subscriptions[$customer_subscription_id] = $subscription;

			//If Subscription is on Hold, check when it should be resumed
			if ($subscription['status'] === self::STATUS_PAUSED) {
				$resume_date = $this->getMeta($customer_subscription_id, 'resume_date');
				$pause_date  = $this->getMeta($customer_subscription_id, 'pause_date');

				$this->subscriptions[$customer_subscription_id]['resume_date'] = $resume_date;
				$this->subscriptions[$customer_subscription_id]['pause_date']  = $pause_date;

				if ($resume_date && ($this->date->isToday($resume_date) || $this->date->isInPast($resume_date))) {
					$this->resumeCustomerSubscription($customer_subscription_id);

					//resuming will invalidate the subscription, so we need to get it again.
					return $this->getCustomerSubscription($customer_subscription_id);
				}
			}
		}

		return $this->subscriptions[$customer_subscription_id];
	}

	public function getCustomerSubscriptionsForCustomer($filter = array(), $select = '', $total = false)
	{
		if ($this->customer->isLogged()) {
			$filter['customer_ids'] = array(customer_info('customer_id'));
		} else {
			return array();
		}

		return $this->getCustomerSubscriptions($filter, $select, $total);
	}

	public function getCustomerSubscriptions($filter = array(), $select = '', $total = false, $index = 'customer_subscription_id')
	{
		//Select
		if ($total) {
			$select = "COUNT(*) as total";
		} elseif (empty($select)) {
			$select = 'cs.*';
		}

		//From
		$from = DB_PREFIX . "customer_subscription cs";

		//Where
		$where = $this->getWhere('customer_subscription', $filter, 'cs');

		if (!empty($filter['customer_ids'])) {
			$where .= " AND cs.customer_id IN (" . implode(',', $this->escape($filter['customer_ids'])) . ")";
		} elseif (isset($filter['customer'])) {
			$select .= ", CONCAT(c.first_name, ' ', c.last_name) as customer";

			$from .= " LEFT JOIN {$this->t['customer']} c ON (c.customer_id=cs.customer_id)";

			$where .= " AND CONCAT(c.first_name, ' ', c.last_name) like '%" . $this->escape($filter['customer']) . "%'";
		}

		if (!empty($filter['statuses'])) {
			$where .= " AND cs.status IN ('" . implode("','", $this->escape($filter['statuses'])) . "')";
		}

		if (!empty($filter['!statuses'])) {
			$where .= " AND cs.status NOT IN ('" . implode("','", $this->escape($filter['!statuses'])) . "')";
		}

		//Order By and Limit
		if (!$total) {
			$order = $this->extractOrder($filter);
			$limit = $this->extractLimit($filter);
		} else {
			$order = '';
			$limit = '';
		}

		//The Query
		$result = $this->queryRows("SELECT $select FROM $from WHERE $where $order $limit", $index, $total);

		if ($total) {
			return $result->row['total'];
		}

		foreach ($result->rows as $key => &$subscription) {
			$subscription = $this->getCustomerSubscription($subscription['customer_subscription_id']);

			if (!$subscription) {
				unset($result->rows[$key]);
			}
		}
		unset($subscription);

		return $result->rows;
	}

	public function getCustomerSubscriptionOptions($customer_subscription_id)
	{
		$option_values = $this->queryRows("SELECT * FROM {$this->t['customer_subscription_option']} WHERE customer_subscription_id = " . (int)$customer_subscription_id);

		$options = array();

		foreach ($option_values as $value) {
			$options[$value['product_option_id']][] = $value['product_option_value_id'];
		}

		return $options;
	}

	public function getTotalCustomerSubscriptions($filter = array())
	{
		return $this->getCustomerSubscriptions($filter, '', true);
	}

	public function invalidate($customer_subscription_id)
	{
		clear_cache("customer_subscription.$customer_subscription_id");
		unset($this->subscriptions[$customer_subscription_id]);
	}

	/********************************
	 * Shipping, Payments & Billing *
	 ********************************/

	public function confirm($cart_key, $data = array())
	{
		$cart_subscription = $this->getCartSubscription($cart_key);

		//if this product is not in the cart, do not process
		if (!$cart_subscription) {
			$this->error['key'] = _l("The subscription was not found in your cart. Please try adding it again.");
		} elseif (empty($cart_subscription['id'])) {
			$this->error['id'] = _l("We were unable to confirm your subscription. Please try subscribing again or <a href=\"%s\">Contact Us</a> to complete your order.", site_url('contact'));
		}

		if (empty($data['shipping_address_id'])) {
			$this->error['shipping_address_id'] = _l("You must specify a shipping address");
		}

		if (empty($data['payment_profile_id'])) {
			$this->error['payment_profile_id'] = _l("Please choose a payment method for this subscription.");
		}

		if ($this->error) {
			return false;
		}

		//Load subscription data
		$data += array(
			'cart_key'        => $cart_key,
			'product_id'      => $cart_subscription['id'],
			'subscription_id' => $cart_subscription['subscription']['subscription_id'],
			'shipping_code'   => !empty($data['shipping']['code']) ? $data['shipping']['code'] : '',
			'shipping_key'    => !empty($data['shipping']['key']) ? $data['shipping']['key'] : '',
			'status'          => self::STATUS_PENDING,
			'date_added'      => $this->date->now(),
			'date_modified'   => $this->date->now(),
			'options'         => $cart_subscription['options'],
		);

		//Add the customer's subscription
		$customer_subscription_id = $this->addCustomerSubscription($data);

		if (!$customer_subscription_id) {
			return false;
		}

		//Handle Quantities
		$this->query("UPDATE {$this->t['product']} SET quantity = (quantity - " . (int)$cart_subscription['quantity'] . ") WHERE product_id = '" . (int)$cart_subscription['product']['product_id'] . "' AND subtract = '1'");

		foreach ($cart_subscription['options'] as $option_values) {
			foreach ($option_values as $subscription_option) {
				$this->query("UPDATE {$this->t['product_option_value']} SET quantity = (quantity - " . (int)$cart_subscription['quantity'] . ") WHERE product_option_value_id = '" . (int)$subscription_option['product_option_value_id'] . "' AND subtract = '1'");
			}
		}

		//Clear this subscription product's cache
		clear_cache('product.' . $cart_subscription['product']['product_id']);

		//Remove the subscription from the cart
		$this->removeFromCart($cart_key);

		//Attempt to activate the subscription
		if (!$this->activateCustomerSubscription($customer_subscription_id)) {
			return false;
		}

		//Charge Subscription
		$this->chargeSubscription($customer_subscription_id);

		return $customer_subscription_id;
	}

	public function chargeSubscription($customer_subscription_id)
	{
		$customer_subscription = $this->getCustomerSubscription($customer_subscription_id);

		if (!$customer_subscription) {
			return false;
		}

		if ($customer_subscription['status'] !== self::STATUS_ACTIVE) {
			return false;
		}

		//If payment is not Due, return True because there are no problems
		if (!$this->paymentDue($customer_subscription_id)) {
			if ($customer_subscription['status'] === self::STATUS_PAUSED) {
				$this->setMeta($customer_subscription_id, 'hold_cycles', $customer_subscription['hold_cycles'] + 1);
			}

			return true;
		}

		$transaction = array(
			'address_id'   => $customer_subscription['payment_address_id'],
			'amount'       => $customer_subscription['total'],
			'payment_code' => $customer_subscription['payment_code'],
			'payment_key'  => $customer_subscription['payment_key'],
			'retries'      => $customer_subscription['retries'],
		);

		$transaction_id = $this->transaction->add('subscription', $transaction);

		$history_data = array(
			'status'         => self::STATUS_ACTIVE,
			'comment'        => _l("Payment Added"),
			'transaction_id' => $transaction_id,
		);

		$this->addHistory($customer_subscription_id, 'payment', $history_data);

		if (!$this->transaction->confirm($transaction_id)) {
			$this->error['payment'] = _l("Payment Failed.");

			return false;
		}

		return true;
	}

	public function chargeSubscriptions()
	{
		$customer_subscriptions = $this->getCustomerSubscriptions();

		foreach ($customer_subscriptions as $customer_subscription) {
			$this->chargeSubscription($customer_subscription['customer_subscription_id']);
		}
	}

	public function cancelDelinquentSubscriptions()
	{
		$query = "SELECT csh.customer_subscription_id FROM {$this->t['customer_subscription_history']} csh" .
			" JOIN {$this->t['transaction']} t ON (t.transaction_id = csh.transaction_id)" .
			" WHERE t.type = 'subscription' AND t.status = '" . Transaction::STATUS_FAILED . "' AND t.retries <= 0";

		$customer_subscription_ids = $this->queryColumn($query);

		foreach ($customer_subscription_ids as $customer_subscription_id) {
			$this->cancelCustomerSubscription($customer_subscription_id);
		}
	}

	/** Payment Scheduling **/

	public function paymentDue($customer_subscription_id)
	{
		$customer_subscription = $this->getCustomerSubscription($customer_subscription_id);

		if (!$customer_subscription) {
			return false;
		}

		$subscription = $customer_subscription['subscription'];

		//Note: Last Payment Date is last payment transaction, not necessarily a successful payment.
		$last_payment = $this->getLastPaymentDate($customer_subscription_id);

		if ($subscription['recurring']) {
			$next_due = $this->nextDueDate($customer_subscription['date_activated'], $last_payment, $subscription['cycles'], $subscription['time'], $subscription['time_unit'], $subscription['day']);

			if ($subscription['status'] === self::STATUS_ACTIVE) {
				return $this->date->isToday($next_due) || $this->date->isInPast($next_due);
			}

			//If Subscription is on hold, and the payment due date is before the time the subscription was put on hold, then payment is still due.
			if ($subscription['status'] === self::STATUS_PAUSED) {
				$pause_date = $this->getMeta($customer_subscription_id, 'pause_date');

				//If the payment due date is before subscription was put on hold
				return $this->date->isBefore($next_due, $pause_date);
			}

			return false;
		} else {
			return empty($last_payment);
		}
	}

	public function getLastPaymentDate($customer_subscription_id)
	{
		return $this->queryVar("SELECT date_added FROM {$this->t['customer_subscription_history']} WHERE customer_subscription_id = " . (int)$customer_subscription_id . " AND type = 'payment' AND transaction_id > 0 ORDER BY date_added DESC");
	}

	public function getNextPaymentDate($customer_subscription_id)
	{
		$customer_subscription = $this->getCustomerSubscription($customer_subscription_id);

		if (!$customer_subscription) {
			return false;
		}

		$subscription = $customer_subscription['subscription'];

		$last_payment = $this->getLastPaymentDate($customer_subscription_id);

		if ($subscription['recurring']) {
			$next_due = $this->nextDueDate($customer_subscription['date_activated'], $last_payment, $subscription['cycles'], $subscription['time'], $subscription['time_unit'], $subscription['day']);

			if ($next_due) {
				return $this->date->isInFuture($next_due) ? $this->date->format($next_due) : $this->date->now();
			}
		} else {
			return empty($last_payment) ? $this->date->now() : false;
		}
	}

	/** Shipping Scheduling **/

	public function shipSubscription($customer_subscription_id)
	{
		$customer_subscription = $this->getCustomerSubscription($customer_subscription_id);

		//If payment is not Due, return True because there are no problems
		if (!$this->shippingDue($customer_subscription_id)) {
			if ($customer_subscription['status'] === self::STATUS_PAUSED) {
				$this->setMeta($customer_subscription_id, 'shipping_hold_cycles', $customer_subscription['shipping_hold_cycles'] + 1);
			}

			return true;
		}

		$shipping = array(
			'address_id'    => $customer_subscription['shipping_address_id'],
			'shipping_code' => $customer_subscription['shipping_code'],
			'shipping_key'  => $customer_subscription['shipping_key'],
			'tracking'      => '',
		);

		$shipping_id = $this->shipping->add('subscription', $shipping);

		$history_data = array(
			'shipping_id' => $shipping_id,
			'comment'     => _l("Subscription Shipment Created"),
		);

		$this->addHistory($customer_subscription_id, 'shipping', $history_data);

		return true;
	}

	public function shipSubscriptions()
	{
		$customer_subscriptions = $this->getCustomerSubscriptions();

		foreach ($customer_subscriptions as $customer_subscription) {
			$this->shipSubscription($customer_subscription['customer_subscription_id']);
		}
	}

	public function shippingDue($customer_subscription_id)
	{
		if (!$this->isActive($customer_subscription_id)) {
			return false;
		}

		$customer_subscription = $this->getCustomerSubscription($customer_subscription_id);
		$subscription          = $customer_subscription['subscription'];

		$last_shipment = $this->getLastShippingDate($customer_subscription_id);

		if ($subscription['shipping_recurring']) {
			$next_due = $this->nextDueDate($customer_subscription['date_activated'], $last_shipment, $subscription['shipping_cycles'], $subscription['shipping_time'], $subscription['shipping_time_unit'], $subscription['shipping_day']);

			if ($next_due && $this->date->isInFuture($next_due) && !$this->date->isToday($next_due)) {
				return false;
			}
		} else {
			return empty($last_shipment);
		}
	}

	public function getLastShippingDate($customer_subscription_id)
	{
		return $this->queryVar("SELECT date_added FROM {$this->t['customer_subscription_history']} WHERE customer_subscription_id = " . (int)$customer_subscription_id . " AND type = 'shipping' AND shipping_id > 0 ORDER BY date_added DESC LIMIT 1");
	}

	public function getNextShippingDate($customer_subscription_id)
	{
		$customer_subscription = $this->getCustomerSubscription($customer_subscription_id);

		if (!$customer_subscription) {
			return false;
		}

		$subscription = $customer_subscription['subscription'];

		$last_shipment = $this->getLastShippingDate($customer_subscription_id);

		if ($subscription['shipping_recurring']) {
			$next_due = $this->nextDueDate($customer_subscription['date_activated'], $last_shipment, $subscription['shipping_cycles'], $subscription['shipping_time'], $subscription['shipping_time_unit'], $subscription['shipping_day']);

			if ($next_due) {
				return $this->date->isInFuture($next_due) ? $this->date->format($next_due) : $this->date->now();
			}
		} else {
			return empty($last_shipment) ? $this->date->now() : null;
		}
	}

	/** Generic Scheduling **/

	//TODO: optimize this!!
	private function nextDueDate($start_date, $from_date, $cycles, $time, $time_unit, $day)
	{
		//If $from_date not given, or is before $start_date, start checking from day before $start_date (in case it is due on $start_date)
		if (!$from_date || $this->date->isBefore($from_date, $start_date)) {
			$from_date = $this->date->add($start_date, '-1 day');
		}

		$this->date->datetime($from_date);

		//Search for the next due date, or if the recurring subscription has ended, return null
		if ($cycles) {
			$due_date = $start_date;

			//TODO: This can be highly optimized
			while ($cycles > 0) {
				if ($this->cycleDueOn($start_date, $due_date, $cycles, $time, $time_unit, $day)) {
					if ($this->date->isAfter($due_date, $from_date) && !$this->date->isSameDay($due_date, $from_date)) {
						return $due_date;
					}

					$cycles--;
				}

				$due_date = $this->date->add($due_date, '1 day');
			}
		} else {
			//Unlimited Cycles, keep checking until we find the next due date
			$due_date = $this->date->add($from_date, '1 day');

			//Limit loop in case of error
			$loop_count = 500;

			while ($loop_count-- > 0) {
				if ($this->cycleDueOn($start_date, $due_date, $cycles, $time, $time_unit, $day)) {
					return $due_date;
				}

				$due_date = $this->date->add($due_date, '1 day');
			}
		}
	}

	private function cycleDueOn($start_date, $on_date, $cycles, $time, $time_unit, $day)
	{
		$this->date->datetime($on_date);

		//Charge Only on a specific day
		if ($day) {
			switch ($time_unit) {
				//Charge on day of week
				case 'W':
					if ($this->date->getDayOfWeek($on_date) !== $day) {
						return false;
					}
					break;

				//Charge on day of month
				case 'M':
					if ($this->date->getDayOfMonth($on_date) !== $day) {
						return false;
					}
					break;

				//Charge on day of year
				case 'Y':
					if ($this->date->getDayOfYear($on_date) !== $day) {
						return false;
					}
					break;
			}
		}

		//Limit Number of Charges from date_added
		if ($cycles) {
			$diff         = $this->date->diff($start_date, $on_date);
			$total_period = $cycles * $time;

			switch ($time_unit) {
				//Charge every $time days
				case 'D':
					if (($diff->days > $total_period) || ($diff->days % $time !== 0)) {
						return false;
					}
					break;

				//Charge every $time weeks until end of period
				case 'W':
					$weeks = $diff->days / 7;

					if (($weeks > $total_period) || ($weeks % $time !== 0)) {
						return false;
					}
					break;

				//Charge every $time months until end of period
				case 'M':
					if ($diff->m > $total_period || ($diff->m % $time !== 0)) {
						return false;
					}
					break;

				//Charge every $time years until end of period
				case 'Y':
					if ($diff->y > $total_period || ($diff->y % $time !== 0)) {
						return false;
					}
					break;
			}
		}

		return true;
	}

	/************************
	 * Subscription Details *
	 ************************/

	public function isActive($customer_subscription_id)
	{
		$customer_subscription = $this->getCustomerSubscription($customer_subscription_id);

		if (!$customer_subscription) {
			return false;
		}

		if ($customer_subscription['status'] === self::STATUS_ACTIVE) {
			return true;
		} //If on hold, subscription is still active until the end of the current cycle
		elseif ($customer_subscription['status'] === self::STATUS_PAUSED) {
			$last_payment = $this->getLastPaymentDate($customer_subscription_id);

			$subscription = $customer_subscription['subscription'];

			$next_due = $this->nextDueDate($customer_subscription['date_activated'], $last_payment, $subscription['cycles'], $subscription['time'], $subscription['time_unit'], $subscription['days']);

			return $this->date->isInFuture($next_due);
		}

		return false;
	}

	//TODO: need to handle multiple subscriptions (eg: free trials...)
	public function getActiveSubscription($subscriptions)
	{
		return current($subscriptions);
	}

	public function getProductSubscription($product_id)
	{
		return $this->getActiveSubscription($this->getProductSubscriptions($product_id));
	}

	public function getProductSubscriptions($product_id)
	{
		$query = "SELECT * FROM {$this->t['product_subscription']} ps" .
			" LEFT JOIN {$this->t['subscription']} s ON (s.subscription_id=ps.subscription_id)" .
			" WHERE s.status = 1 AND product_id = " . (int)$product_id . " ORDER BY sort_order";

		return $this->queryRows($query);
	}

	public function fillCustomerSubscriptionDetails(&$subscription)
	{
		$subscription['id'] = $subscription['product_id'];

		$options = $this->getCustomerSubscriptionOptions($subscription['customer_subscription_id']);

		if (!$this->fillSubscriptionDetails($subscription, $options)) {
			return false;
		}

		return true;
	}

	public function fillSubscriptionDetails(&$subscription, $options = array())
	{
		$product = $this->Model_Product->getProduct($subscription['id']);

		if (!$product) {
			return false;
		}

		$product['options']       = $this->Model_Product->getProductOptions($subscription['id']);
		$product['subscriptions'] = $this->getProductSubscriptions($subscription['id']);

		$subscription['product'] = $product;

		if ($options || empty($subscription['options'])) {
			$subscription['options'] = $options;
		}

		if (!$this->Model_Product->fillProductDetails($subscription, $subscription['id'], 1, $subscription['options'], $ignore_status = true)) {
			$this->error += $this->Model_Product->getError();
			return false;
		}

		$subscription['subscription'] = $this->getActiveSubscription($subscription['product']['subscriptions']);

		//TODO: Handle prorated charges
		$subscription['prorate'] = true;

		if (!$this->calculateSubscriptionTotal($subscription)) {
			return false;
		}

		$subscription['total_display'] = $this->formatPrice($subscription['subscription'], $subscription['total']);

		return true;
	}

	public function getSubscriptionTotal($product_id, $options)
	{
		$subscription['id'] = $product_id;

		if (!$this->fillSubscriptionDetails($subscription, $options)) {
			return false;
		}

		return $subscription['total'];
	}

	public function calculateSubscriptionTotal(&$subscription)
	{
		$types = $this->Model_Subscription->getSubscriptionTypes();

		foreach ($types as $type) {
			if (method_exists($type, 'calculateTotal')) {
				if (!$type->calculateTotal($subscription)) {
					$this->error += $type->getError();
					return false;
				}
			}
		}

		return true;
	}

	/**********************
	 * Subscription Tools *
	 **********************/

	public function formatPrice($subscription, $price)
	{
		if (is_integer($subscription) || is_string($subscription)) {
			$subscription = $this->getSubscription((int)$subscription);
		}

		if (!$subscription) {
			return '';
		}

		if ($subscription['time'] > 1) {
			$sub_text = ' ' . $subscription['time'] . ' ' . self::$time_units[$subscription['time_unit']] . 's';
		} else {
			$sub_text = ' ' . self::$time_units[$subscription['time_unit']];
		}

		return $this->currency->format($price) . ' / ' . $sub_text;
	}

	public function getMeta($customer_subscription_id, $key = null)
	{
		if (!$key) {
			$metadata = $this->queryRows("SELECT * FROM {$this->t['customer_subscription_meta']} WHERE customer_subscription_id = " . (int)$customer_subscription_id, 'key');

			foreach ($metadata as &$meta) {
				if ($meta['serialized']) {
					$meta['value'] = unserialize($meta['value']);
				}
			}

			return $metadata;
		} else {
			$metadata = $this->queryRow("SELECT * FROM {$this->t['customer_subscription_meta']} WHERE customer_subscription_id = " . (int)$customer_subscription_id . " AND `key` = '" . $this->escape($key) . "'");

			if ($metadata) {
				if ($metadata['serialized']) {
					return unserialize($metadata['value']);
				}

				return $metadata['value'];
			}
		}

		return null;
	}

	public function setMeta($customer_subscription_id, $key, $value)
	{
		//Serialize if necessary
		if (is_array($value) || is_object($value)) {
			$entry_value = serialize($value);
			$serialized  = 1;
		} else {
			$entry_value = $value;
			$serialized  = 0;
		}

		$data = array(
			'customer_subscription_id' => $customer_subscription_id,
			'key'                      => $key,
			'value'                    => $entry_value,
			'serialized'               => $serialized,
		);

		$where = array(
			'customer_subscription_id' => $customer_subscription_id,
			'key'                      => $key,
		);

		$this->delete('customer_subscription_meta', $where);
		$this->insert('customer_subscription_meta', $data);
	}

	public function deleteMeta($customer_subscription_id, $key)
	{
		$where = array(
			'customer_subscription_id' => $customer_subscription_id,
			'key'                      => $key,
		);

		$this->delete('customer_subscription_meta', $where);
	}

	public function addHistory($customer_subscription_id, $type, $history_data)
	{
		$history_data += array(
			'customer_subscription_id' => $customer_subscription_id,
			'type'                     => $type,
			'date_added'               => $this->date->now(),
		);

		if (!isset($history_data['status'])) {
			$history_data['status'] = $this->queryVar("SELECT status FROM {$this->t['customer_subscription']} WHERE customer_subscription_id = " . (int)$customer_subscription_id);
		}

		if (isset($history_data['data'])) {
			$history_data['data'] = serialize($history_data['data']);
		}

		$this->insert('customer_subscription_history', $history_data);
	}

	public function getHistory($customer_subscription_id)
	{
		$history = $this->queryRows("SELECT * FROM {$this->t['customer_subscription_history']} WHERE customer_subscription_id = " . (int)$customer_subscription_id . " ORDER BY date_added DESC, customer_subscription_history_id DESC");

		foreach ($history as &$history_data) {
			if (!empty($history_data['data'])) {
				$history_data['data'] = unserialize($history_data['data']);
			}
		}
		unset($history_data);

		return $history;
	}

	/** Verification **/
	private function verifyCustomerSubscription($customer_subscription_id)
	{
		//If we have already loaded the subscription, it has already been verified
		if (isset($this->subscriptions[$customer_subscription_id])) {
			return true;
		}

		//check if user has privileges, if not, check if the subscription belongs to this customer
		if (!user_can('w', 'customer/subscription')) {
			$verified = false;

			if ($this->customer->isLogged()) {
				$customer_id = (int)customer_info('customer_id');

				$verified = $this->queryVar("SELECT COUNT(*) FROM {$this->t['customer_subscription']} WHERE customer_subscription_id = " . (int)$customer_subscription_id . " AND customer_id = $customer_id");
			}

			if (!$verified) {
				$this->error['customer'] = _l("Unable to find the requested subscription");

				if (!empty($customer_id)) {
					write_log('error', "Customer ID $customer_id attempted to access unassociated subscription with customer_subscription_id $customer_subscription_id!");
				}

				return false;
			}
		}

		return true;
	}
}
