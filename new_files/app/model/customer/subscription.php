<?php

class App_Model_Customer_Subscription extends App_Model_Table
{
	protected $table = 'customer_subscription', $primary_key = 'customer_subscription_id';

	public function getSubscriptions($sort = array(), $filter = array(), $options = array(), $total = false)
	{
		$subscriptions = $this->getRecords($sort, $filter, $options, $total);

		foreach ($subscriptions as $subscription_id => &$subscription) {
			$subscription = $this->subscription->getCustomerSubscription($subscription['customer_subscription_id']);

			if (!$subscription) {
				unset($subscriptions[$subscription['customer_subscription_id']]);
			}
		}
		unset($subscription);

		return $subscriptions;
	}

	public function getColumns($filter = array())
	{
		$columns['customer'] = array(
			'display_name' => _l('Customer'),
			'type'         => 'text',
			'filter'       => true,
			'sortable'     => true,
		);

		$columns['shipping_due'] = array(
			'display_name' => _l('Next Shipment Due'),
			'type'         => 'text',
		);

		$columns['shipping_address'] = array(
			'display_name' => _l('Shipping Address'),
			'type'         => 'text',
		);

		$columns['status'] = array(
			'display_name' => _l('Status'),
			'type'         => 'select',
			'build_data'   => Subscription::$statuses,
			'filter'       => true,
			'sortable'     => true,
		);

		return $this->getTableColumns($this->table, $columns, $filter);
	}

	public function getViewListingId()
	{
		$view_listing = $this->Model_ViewListing->getViewListingBySlug('customer_subscription_list');

		if (!$view_listing) {
			$view_listing = array(
				'name' => _l("Customer Subscriptions"),
				'slug' => 'customer_subscription_list',
				'path' => 'admin/customer/subscription/listing',
			);

			$view_listing_id = $this->Model_ViewListing->save(null, $view_listing);
		} else {
			$view_listing_id = $view_listing['view_listing_id'];
		}

		return $view_listing_id;
	}
}
