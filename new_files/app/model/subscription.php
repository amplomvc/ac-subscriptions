<?php

class App_Model_Subscription extends App_Model_Table
{
	protected $table = 'subscription', $primary_key = 'subscription_id';

	public function save($subscription_id, $subscription)
	{
		if (isset($subscription['name'])) {
			if (!validate('text', $subscription['name'], 2, 255)) {
				$this->error['name'] = _l("The Subscription Name must be between 2 and 255 characters!");
			}
		} elseif (!$subscription_id) {
			$this->error['name'] = _l("The Subscription Name is required.");
		}

		if (isset($subscription['code'])) {
			if (!validate('text', $subscription['code'], 2, 45)) {
				$this->error['code'] = _l("The Subscription Code must be between 2 and 45 characters!");
			}
		} elseif (!$subscription_id) {
			$subscription['code'] = $this->generateCode(null, $subscription['name']);
		}

		if ($subscription_id) {
			clear_cache("subscription.$subscription_id");

			$this->update('subscription', $subscription, $subscription_id);

			if ($this->hasSubscribers($subscription_id)) {
				clear_cache("customer_subscription");
			}
		} else {
			$subscription['date_added'] = $this->date->now();

			$subscription_id = $this->insert('subscription', $subscription);
		}

		clear_cache('subscriptions');

		return $subscription_id;
	}

	public function hasSubscribers($subscription_id)
	{
		$query = "SELECT COUNT(*) FROM {$this->t['product_subscription']} ps" .
			" LEFT JOIN {$this->t['customer_subscription']} cs ON (ps.product_id=cs.product_id)" .
			" WHERE ps.subscription_id = " . (int)$subscription_id . " AND status IN ('" . Subscription::STATUS_ACTIVE . "', '" . Subscription::STATUS_PAUSED . "')";

		return $this->queryVar($query);
	}

	public function isRemovable($subscription_id)
	{
		return $this->queryVar("SELECT COUNT(*) FROM {$this->t['product_subscription']} WHERE subscription_id = " . (int)$subscription_id);
	}

	public function remove($subscription_id)
	{
		if (!$this->isRemoveable($subscription_id)) {
			$subscription                                   = $this->Model_Subscription->getSubscription($subscription_id);
			$this->error['subscription' . $subscription_id] = _l("Unable to delete the Subscription, %s, because it is associated to a product!", $subscription['name']);
			return false;
		}

		$this->delete('subscription', $subscription_id);

		clear_cache("subscription.$subscription_id");

		return true;
	}

	public function generateCode($subscription_id, $name)
	{
		$model = strtoupper($this->url->format($name));

		$count        = 1;
		$unique_model = $model;

		while ($this->queryVar("SELECT COUNT(*) FROM {$this->t['subscription']} WHERE code like '" . $this->escape($unique_model) . "' AND subscription_id != " . (int)$subscription_id)) {
			$unique_model = $model . '-' . $count++;
		}

		return $unique_model;
	}

	public function getColumns($filter = array())
	{
		$columns = array(
			'plan'          => array(
				'type'         => 'text',
				'display_name' => _l("Subscription Plan"),
				'filter'       => false,
				'sortable'     => false,
			),
			'shipping_plan' => array(
				'type'         => 'text',
				'display_name' => _l("Shipping Plan"),
				'filter'       => false,
				'sortable'     => false,
			),
			'status'        => array(
				'type'         => 'select',
				'display_name' => _l("Status"),
				'build_data'   => array(
					0 => _l("Disabled"),
					1 => _l("Enabled"),
				),
				'filter'       => true,
				'sortable'     => true,
			),
		);

		return $this->getTableColumns('subscription', $columns, $filter);
	}

	public function getSubscriptionTypes()
	{
		$type_files = get_files(DIR_SITE . 'app/model/subscription/', array('php'));

		$types = array();

		if (!empty($type_files)) {
			foreach ($type_files as &$file) {
				$name = $file->getBasename('.php');
				$class = "App_Model_Subscription_" . ucfirst($name);

				if (!class_exists($class)) {
					trigger_error(_l("Could not find class %s. Please make sure you have named your Subscription Type Class correctly in %s", $class, $file->getPathName()));
					exit;
				}

				$types[$name] = new $class;
			}
		}

		return $types;
	}
}
