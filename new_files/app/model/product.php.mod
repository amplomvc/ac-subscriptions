#<?php

//=====
class App_Model_Product extends App_Model_Table
{
//.....
	public function save($product_id, $product, $strict = false)
	{
//-----
//>>>>> {php} {before}
		if (isset($product['product_subscriptions'])) {
			$this->delete('product_subscription', array('product_id' => $product_id));

			$sort_order = 0;

			foreach (array_unique_keys($product['product_subscriptions'], 'subscription_id') as $subscription) {
				$subscription['product_id'] = $product_id;
				$subscription['sort_order'] = $sort_order++;

				$this->insert('product_subscription', $subscription);
			}
		}
//-----
//=====
		clear_cache("product.$product_id");
//-----
//=====
	}
//.....
	public function copy($product_id)
	{
//.....
		$product['product_templates']  = $this->getProductTemplates($product_id);
//-----
//>>>>> {php}
		$product['product_subscriptions'] = $this->getProductSubscriptions($product_id);
//-----
//=====
	}
//.....
	public function remove($product_id)
	{
//.....
		$this->delete('review', array('product_id' => $product_id));
//-----
//>>>>> {php}
		$this->delete('product_subscription', array('product_id' => $product_id));
//-----
//=====
	}
//-----
//>>>>> {php} {before}
	public function getProductSubscriptions($product_id)
	{
		$query = "SELECT * FROM {$this->t['product_subscription']} ps" .
			" LEFT JOIN {$this->t['subscription']} s ON (s.subscription_id=ps.subscription_id)" .
			" WHERE product_id = " . (int)$product_id . " ORDER BY sort_order";

		return $this->queryRows($query);
	}
//-----
//=====
	public function getTotalProducts($data = array())
	{
//.....
	}
//.....
}
//-----
