<?php
class App_Controller_Extension_Cart_Subscription extends Controller
{
	public function renderCart()
	{
		//If no subscriptions do nothing
		if ($this->subscription->cartEmpty()) {
			return;
		}

		if ($this->route->getSegment(0) === 'checkout') {
			$data['is_checkout'] = true;
			$data['url_cart']    = site_url('cart');
		} else {
			if (IS_POST) {
				if (!empty($_POST['subscription_remove'])) {
					$subscription = $this->subscription->getCartSubscription($_POST['subscription_remove']);

					$this->subscription->removeFromCart($_POST['subscription_remove']);

					$url_subscription = site_url('product/product', 'product_id=' . $subscription['id']);
					message('success', _l("<a href=\"%s\">%s</a> has been removed from your cart.", $url_subscription, $subscription['product']['name']));
				}
			}

			$subscriptions = $this->subscription->getCart();

			$image_width  = option('config_image_cart_width');
			$image_height = option('config_image_cart_height');

			foreach ($subscriptions as $key => &$subscription) {
				$this->subscription->fillSubscriptionDetails($subscription, $subscription['options']);

				if (!$subscription) {
					$url_subscription = site_url('product/product', 'product_id=' . $subscription['id']);
					message('warning', _l("There was a problem with your subscription for <a href=\"%s\">%s</a>. Please try again.", $url_subscription, $subscription['product']['name']));

					unset($subscriptions[$key]);
					$this->subscription->removeFromCart($key);

					continue;
				}

				$subscription['href'] = site_url('product/product', 'product_id=' . $subscription['product']['product_id']);

				$subscription['thumb'] = image($subscription['product']['image'], $image_width, $image_height);

				$subscription['url_subscribe'] = site_url('checkout/subscribe', 'key=' . $subscription['key']);
				$subscription['remove']        = site_url('cart/cart/remove', 'key=' . $subscription['key']);
			}
			unset($subscription);

			//If we are now empty, reload the page, in case cart empty, or other cart updates necessary
			if (empty($subscriptions)) {
				if ($this->is_ajax) {
					$this->request->redirectBrowser(site_url('cart'));
				}

				redirect('cart');
			}

			$data['subscriptions'] = $subscriptions;
		}

		//Render
		$this->render('block/cart/subscription', $data);
	}

	public function remove()
	{
		$cart_subscription = $this->subscription->getCartSubscription($_GET['key']);

		if ($this->subscription->removeFromCart($_GET['key'])) {
			message('success', _l('<a href="%s">%s</a> has been removed from your cart.', site_url('product/product', 'product_id=' . $cart_subscription['id']), $cart_subscription['product']['name']));
		} else {
			message('error', $this->cart->getError());
		}

		if ($this->is_ajax) {
			output_message();
		} else {
			redirect('cart');
		}
	}
}
