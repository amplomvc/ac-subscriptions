<?php

class App_Controller_Checkout_Subscribe extends Controller
{
	public function index()
	{
		if (!$this->cart->validate()) {
			message('warning', $this->cart->getError());
			redirect('cart');
		}

		//Validate Customer is Logged in
		if (!$this->customer->isLogged()) {
			message('notify', _l("You must login or register first to sign up for a subscription."));
			redirect('customer/login', array('redirect' => $this->url->here()));
		}

		$subscription = !empty($_GET['key']) ? $this->subscription->getCartSubscription($_GET['key']) : null;

		if (!$subscription) {
			message('warning', _l('The requested subscription could not be found. Please sign up for a different subscription plan.'));
			redirect('product/boxes');
		}

		//Page Head
		set_page_info('title', _l("Subscription Confirmation"));

		//Breadcrumbs
		breadcrumb(_l("Home"), site_url());
		breadcrumb(_l("Cart"), site_url('cart'));
		breadcrumb(_l("Subscription Confirmation"), site_url('checkout/subscribe'));

		//Load Information
		$data = array(
			'cart_key' => $_GET['key'],
		);

		$data['subscription'] = $subscription;

		//Template Data
		$data['data_shipping_addresses'] = $this->customer->getShippingAddresses();
		$data['data_payment_addresses'] = $this->customer->getPaymentAddresses();

		$data['shipping_address_id'] = $this->customer->getDefaultShippingAddressId();
		$data['payment_address_id'] = $this->customer->getDefaultPaymentAddressId();

		//Subscribe From
		$this->cart->setPaymentMethod('braintree');

		//Action Buttons
		$data['subscribe']   = site_url('checkout/subscribe/subscribe');

		//Response
		output($this->render('checkout/subscribe', $data));
	}

	public function subscribe()
	{
		if (empty($_POST['key'])) {
			redirect('cart');
		}

		$subscription_id = $this->subscription->confirm($_POST['key'], $_POST);

		if (!$subscription_id) {
			message('warning', 'failed');
			message('warning', $this->subscription->getError());
			redirect('checkout/subscribe', 'key=' . $_POST['key']);
		} else {
			redirect('checkout/subscribe/success', 'subscription_id=' . $subscription_id);
		}
	}

	public function success($data = array())
	{
		//Page Head
		set_page_info('title', _l("Subscription Confirmation"));

		//Template Data
		$customer_subscription = $this->subscription->getCustomerSubscription(_get('subscription_id'));

		if ($customer_subscription) {
			$data += $customer_subscription;
		}

		//Render
		output($this->render('checkout/success', $data));
	}
}
