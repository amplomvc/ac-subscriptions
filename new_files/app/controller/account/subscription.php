<?php

class App_Controller_Account_Subscription extends Controller
{
	static $allow = array(
		'access' => '.*',
	);

	public function index()
	{
		//Page Head
		set_page_info('title', _l('My Subscriptions'));

		//Breadcrumbs
		breadcrumb(_l("Home"), site_url());
		breadcrumb(_l("Account"), site_url('account'));
		breadcrumb(_l("Subscriptions"), site_url('account/subscription'));

		$sort = array(
			'date_activated' => 'ASC',
		);

		$filter = array(
			'!status' => array(
				Subscription::STATUS_CANCELLED,
				Subscription::STATUS_PENDING,
			),
		);

		$subscriptions = $this->Model_Customer_Subscription->getSubscriptions($sort, $filter, array('index' => 'customer_subscription_id'));

		if (!empty($subscriptions)) {
			reset($subscriptions);
			$sub = current($subscriptions);

			foreach ($sub['product']['options'] as $option) {
				$options[$option['name']] = array_column($option['product_option_values'], 'value');
			}

			$data['options'] = $options;
		}

		$data['subscriptions'] = $subscriptions;

		$filter = array(
			'country_id' => option('config_country_id', 223),
		);

		$data['data_zones'] = $this->Model_Localisation_Zone->getRecords(null, $filter, array('cache' => true));

		$content = $this->render('account/subscription/list', $data);

		if ($this->is_ajax) {
			output($content);
		} else {
			$data['content'] = $content;
			output($this->render('account/account', $data));
		}
	}

	public function form()
	{
		$customer_subscription_id = _get('subscription_id');

		//If no subscription, redirect to standard account management
		if (!$customer_subscription_id) {
			redirect('account');
		}

		$subscription = $this->subscription->getCustomerSubscription($customer_subscription_id);

		if (!$subscription) {
			$contact = site_url("page", 'page_id=' . option('config_contact_page_id'));
			message('warning', _l("The requested subscription was not found. It may be Expired or Cancelled. Please <a href=\"$contact\">contact us</a> for further assitance."));
			redirect('account');
		}

		//Page Head
		set_page_info('title', _l('Subscription Management'));

		//Breadcrumbs
		breadcrumb(_l("Home"), site_url());
		breadcrumb(_l("Account"), site_url('account'));
		breadcrumb(_l("Subscriptions"), site_url('account/subscription/form', 'subscription_id=' . $customer_subscription_id));

		//Load Data or Defaults
		$active_statuses = array(
			Subscription::STATUS_PAUSED,
			Subscription::STATUS_ACTIVE
		);

		if (in_array($subscription['status'], $active_statuses)) {
			foreach ($subscription['product']['options'] as &$option) {
				if ($option['group_type'] === 'single') {
					$option_value       = array_search_key('product_option_id', $option['product_option_id'], $subscription['options']);
					$option['selected'] = $option_value['product_option_value_id'];
				} else {
					$option['selected'] = array();

					if (!empty($subscription['options'][$option['product_option_id']])) {
						foreach ($subscription['options'][$option['product_option_id']] as $selected) {
							$option['selected'][] = $selected['product_option_value_id'];
						}
					}
				}
			}
			unset($option);

			//Shipping Address
			$data['data_addresses'] = $this->customer->getShippingAddresses();

		} else {
			switch ($subscription['status']) {
				case Subscription::STATUS_EXPIRED:
					$action        = site_url('account/subscription/renew', 'subscription_id=' . $customer_subscription_id);
					$button_action = _l("Renew You Expired Subscription");
					break;

				default:
					redirect('account');
			}

			$data['action']        = $action;
			$data['button_action'] = $button_action;
		}

		$data['subscription'] = $subscription;

		//Action Buttons
		$data['save']        = site_url('account/subscription/update', 'subscription_id=' . $customer_subscription_id);
		$data['cancel']      = site_url('account/subscription/cancel_form', 'subscription_id=' . $customer_subscription_id);
		$data['put_on_hold'] = site_url('account/subscription/put_on_hold', 'subscription_id=' . $customer_subscription_id);

		//Render
		output($this->render('account/subscription/list', $data));
	}

	public function save()
	{
		$customer_subscription_id = _request('subscription_id');

		if (_post('reactivate')) {
			$this->subscription->resumeCustomerSubscription($customer_subscription_id);
		}

		$subscription = $_POST;

		$box_id = $this->Model_Customer_Subscription->getField($customer_subscription_id, 'product_id');

		$subscription['options'] = $this->smoothiesy->mapBoxOptions($box_id, $_POST);

		unset($subscription['weekly']);
		unset($subscription['people']);
		unset($subscription['superfood']);
		unset($subscription['protein']);

		if ($this->subscription->updateCustomerSubscription($customer_subscription_id, $subscription)) {
			message('success', _l('You have successfully updated your subscription!'));
		} else {
			message('error', $this->subscription->getError());
			message('error', 'payment failed.');
		}

		if ($this->is_ajax) {
			output_message();
		} else {
			redirect('account/subscription');
		}
	}

	public function calculate_total()
	{
		foreach ($_POST['options'] as &$option) {
			if (!is_array($option)) {
				$option = array($option);
			}
		}

		$subscription['id'] = _get('product_id');
		$options            = _post('options', array());

		if (!$this->subscription->fillSubscriptionDetails($subscription, $options)) {
			return false;
		}

		output(!empty($subscription['total_with_savings']) ? $subscription['total_with_savings'] : $subscription['total_display']);
	}

	public function resume()
	{
		$customer_subscription_id = _get('subscription_id', 0);

		if ($this->subscription->resumeCustomerSubscription($customer_subscription_id)) {
			message('success', _l("The subscription has been resumed."));
		} else {
			message('error', $this->subscription->getError());
		}

		if ($this->is_ajax) {
			output_message();
		} else {
			redirect('account/subscription', 'subscription_id=' . $customer_subscription_id);
		}

	}

	public function cancel_form()
	{
		$customer_subscription_id = _get('subscription_id');

		$subscription = $this->subscription->getCustomerSubscription($customer_subscription_id);

		if (!$subscription) {
			message('error', _l("Unable to locate subscription to cancel!"));
			redirect('account/subscription');
		}

		//Page Head
		set_page_info('title', _l("Subscription Cancellation"));

		//Breadcrumbs
		breadcrumb(_l("Home"), site_url());
		breadcrumb(_l("Account"), site_url('account'));
		breadcrumb(_l("Subscriptions"), site_url('account/subscription', 'subscription_id=' . $customer_subscription_id));
		breadcrumb(_l("Cancellation"), site_url('account/subscription/cancel-form', 'subscription_id=' . $customer_subscription_id));

		//Template Data
		$data['customer_subscription_id'] = $customer_subscription_id;
		$data['subscription']             = $subscription;

		//Render
		output($this->render('account/subscription/cancel', $data));
	}

	public function cancel()
	{
		$customer_subscription_id = _get('subscription_id', 0);

		if ($this->subscription->cancelCustomerSubscription($customer_subscription_id)) {
			message('success', _l("We're sorry to see you go! If there is anything we can do to further assist you, please feel free to <a href=\"%s\">contact us.</a>", site_url('contact')));
		} else {
			message('error', $this->subscription->getError());
		}

		if ($this->is_ajax) {
			output_message();
		} else {
			redirect('account/subscription');
		}
	}

	public function pause_form()
	{
		$customer_subscription_id = _get('subscription_id', 0);

		//Page Head
		set_page_info('title', _l("Put on Hold"));

		//Breadcrumbs
		breadcrumb(_l("Home"), site_url());
		breadcrumb(_l("Account"), site_url('account'));
		breadcrumb(_l("Subscriptions"), site_url('account/subscription', 'subscription_id=' . $customer_subscription_id));
		breadcrumb(_l("Pause"), site_url('account/subscription/pause-form', 'subscription_id=' . $customer_subscription_id));

		$subscription = $this->subscription->getCustomerSubscription($customer_subscription_id);

		$data['customer_subscription_id'] = $customer_subscription_id;
		$data['subscription']             = $subscription;

		//Default
		$data['pause_cycles'] = 1;

		$data['data_pause_cycles'] = array(
			1 => _l("1 Cycle"),
			2 => _l("2 Cycles"),
			3 => _l("3 Cycles"),
		);

		//Render
		output($this->render('account/subscription/pause', $data));
	}

	public function pause()
	{
		$customer_subscription_id = _get('subscription_id', 0);

		$resume_date = $this->subscription->pauseCustomerSubscription($customer_subscription_id, $_POST['pause_cycles']);

		if ($resume_date) {
			message("success", _l("Your Subscription is now On Hold and will resume on %s.", format('date', $resume_date, 'long')));
		} else {
			message('error', $this->subscription->getError());
		}

		if ($this->is_ajax) {
			output_message();
		} else {
			redirect('account/subscription');
		}
	}

	public function remove()
	{
		$customer_subscription_id = _get('subscription_id', 0);

		if ($this->subscription->removeCustomerSubscription($customer_subscription_id)) {
			message('error', _l("Your Subscription has been removed from your account."));
		} else {
			message('error', $this->subscription->getError());
		}

		if ($this->is_ajax) {
			output_message();
		} else {
			redirect('account/subscription');
		}
	}
}
