<?php

class App_Controller_Admin_Customer_Subscription extends Controller
{
	public function index()
	{
		//Page Head
		page_info('title', _l('Customer Subscriptions'));

		//Breadcrumbs
		breadcrumb(_l('Home'), site_url('admin'));
		breadcrumb(_l('Customer Subscriptions'), site_url('admin/customer/subscription'));

		//Listing
		$data['listing'] = $this->listing();

		//Batch Actions
		$actions = array(
			'enable'  => array(
				'label' => _l("Enable")
			),
			'disable' => array(
				'label' => _l("Disable"),
			),
			'delete'  => array(
				'label' => _l("Delete"),
			),
		);

		$data['batch_action'] = array(
			'actions' => $actions,
			'url'     => site_url('admin/user/batch-action'),
		);

		//Response
		output($this->render('customer/subscription/list', $data));
	}

	public function listing()
	{
		$required_cols = array(
			'customer_subscription_id' => 1,
			'shipping_address_id'      => 1,
			'status'                   => 1,
			'subscription_id'          => 1,
			'product_id'               => 1,
			'customer_id'              => 1,
		);

		$columns = $this->Model_Customer_Subscription->getColumns((array)_request('columns'));

		$sort    = (array)_request('sort', array('customer_subscription_id' => 'DESC'));
		$filter  = (array)_request('filter');
		$options = array(
			'index'   => 'customer_subscription_id',
			'page'    => _get('page', 1),
			'limit'   => _get('limit', option('admin_list_limit', 20)),
			'columns' => $columns + $required_cols,
		);

		list($customer_subscriptions, $total) = $this->Model_Customer_Subscription->getRecords($sort, $filter, $options, true);

		$image_width  = option('config_image_admin_list_width');
		$image_height = option('config_image_admin_list_height');

		foreach ($customer_subscriptions as $customer_subscription_id => &$customer_subscription) {
			$customer_subscription['actions'] = array(
				'edit' => array(
					'text' => _l('View'),
					'href' => site_url('admin/customer/subscription/view', 'customer_subscription_id=' . $customer_subscription_id)
				),
			);

			switch ($customer_subscription['status']) {
				case Subscription::STATUS_ACTIVE:
					$customer_subscription['actions']['cancel'] = array(
						'text' => _l('Cancel'),
						'href' => site_url('admin/customer/subscription/cancel', 'customer_subscription_id=' . $customer_subscription_id)
					);
					break;

				default:
					$customer_subscription['actions']['activate'] = array(
						'text' => _l('Activate'),
						'href' => site_url('admin/customer/subscription/activate', 'customer_subscription_id=' . $customer_subscription_id)
					);
					$customer_subscription['actions']['delete']   = array(
						'text' => _l('Permanently Delete'),
						'href' => site_url('admin/customer/subscription/remove', 'customer_subscription_id=' . $customer_subscription_id)
					);
					break;
			}

			//Customer
			if (isset($columns['customer'])) {
				$customer = $this->Model_Customer->getRecord($customer_subscription['customer_id']);

				if ($customer) {
					$customer_subscription['customer'] = $customer['first_name'] . ' ' . $customer['last_name'] . '<br>(' . $customer['email'] . ')';
				}
			}

			if (isset($customer_subscription['total'])) {
				$customer_subscription['total'] = $this->subscription->formatPrice($customer_subscription['subscription_id'], $customer_subscription['total']);
			}

			//Product
			if (isset($columns['name']) || isset($columns['image'])) {
				$product = $this->Model_Product->getProduct($customer_subscription['product_id']);

				if (!$product) {
					message('warning', _l("Subscription associated with unknown product! PID: %s, SID: %s", $customer_subscription['product_id'], $customer_subscription['customer_subscription_id']));
				} else {
					$customer_subscription['name']  = $product['name'];
					$customer_subscription['image'] = image($product['image'], $image_width, $image_height);
				}
			}

			if (isset($customer_subscription['date_activated'])) {
				$customer_subscription['date_activated'] = $customer_subscription['date_activated'] === DATETIME_ZERO ? _l("Never") : $this->date->format($customer_subscription['date_activated'], 'datetime_long');
			}

			if (isset($columns['shipping_address'])) {
				$customer_subscription['shipping_address'] = format('address', $customer_subscription['shipping_address_id']);
			}

			if (isset($columns['shipping_due'])) {
				$customer_subscription['shipping_due'] = _l("N/A");

				$subscription = $this->Model_Subscription->getRecord($customer_subscription['subscription_id']);

				if ($subscription && $subscription['shipping_recurring']) {
					if ($this->subscription->shippingDue($customer_subscription['customer_subscription_id'])) {
						$customer_subscription['shipping_due'] = _l("<a href=\"%s\" class=\"button shipnow\">Ship Now</a>", site_url('admin/customer/subscription/ship', 'customer_subscription_id=' . $customer_subscription_id));
					} else {
						$customer_subscription['shipping_due'] = format('date', $this->subscription->getNextShippingDate($customer_subscription['customer_subscription_id']), 'medium');
					}
				}
			}
		}
		unset($customer_subscription);

		$listing = array(
			'columns'        => $columns,
			'extra_cols'     => $this->Model_Customer_Subscription->getColumns(false),
			'records'        => $customer_subscriptions,
			'sort'           => $sort,
			'filter_value'   => $filter,
			'pagination'     => true,
			'total_listings' => $total,
			'listing_path'   => 'admin/customer/subscription/listing',
			'save_path'      => 'admin/customer/subscription/save',
		);

		$output = block('widget/listing', null, $listing + $options);

		//Response
		if ($this->is_ajax) {
			output($output);
		}

		return $output;
	}

	public function save()
	{
		if ($customer_subscription_id = $this->Model_Customer_Subscription->save(_request('customer_subscription_id'), $_POST)) {
			message('success', _l("The Customer Subscription has been saved."));
			message('data', array('customer_subscription_id' => $customer_subscription_id));
		} else {
			message('error', $this->Model_Customer_Subscription->fetchError());
		}

		if ($this->is_ajax) {
			output_message();
		} elseif ($this->message->has('error')) {
			post_redirect('admin/customer/subscription/form');
		} else {
			redirect('admin/customer/subscription');
		}
	}

	public function activate()
	{
		if (!empty($_GET['subscription_id'])) {
			if ($this->subscription->activateCustomerSubscription($_GET['subscription_id'])) {
				message('success', _l("The subscription has been activated!"));
			} else {
				message('error', $this->subscription->getError());
			}
		}

		if ($this->is_ajax) {
			output_message();
		} else {
			redirect('admin/customer/subscription');
		}
	}

	public function cancel()
	{
		if (!empty($_GET['subscription_id'])) {
			if ($this->subscription->cancelCustomerSubscription($_GET['subscription_id'])) {
				message('success', _l("The subscription has been cancelled."));
			} else {
				message('error', $this->subscription->getError());
			}
		}

		if ($this->is_ajax) {
			output_message();
		} else {
			redirect('admin/customer/subscription');
		}
	}

	public function remove()
	{
		if (!empty($_GET['subscription_id'])) {
			if ($this->subscription->removeCustomerSubscription($_GET['subscription_id'])) {
				message('notify', _l("The subscription has been removed from the system!"));
			} else {
				message('error', $this->subscription->getError());
			}
		}

		if ($this->is_ajax) {
			output_message();
		} else {
			redirect('admin/customer/subscription');
		}
	}

	public function form()
	{
		//Page Head
		set_page_info('title', _l("Subscription"));

		//Insert or Update
		$customer_subscription_id = _get('customer_subscription_id');

		//Breadcrumbs
		breadcrumb(_l("Home"), site_url('admin'));
		breadcrumb(_l("Subscription"), site_url('admin/customer/subscription'));
		breadcrumb($customer_subscription_id ? _l("Edit") : _l("Add"), site_url('admin/customer/subscription/form', 'subscription_id=' . $customer_subscription_id));

		//Load Information
		if ($customer_subscription_id && !IS_POST) {
			$customer_subscription = $this->Model_Customer_Subscription->getRecord($customer_subscription_id);
		} else {
			$customer_subscription = $_POST;
		}

		//Set Values or Defaults
		$defaults = array(
			'parent_id'        => 0,
			'name'             => '',
			'description'      => '',
			'meta_keywords'    => '',
			'meta_description' => '',
			'alias'            => '',
			'image'            => '',
			'sort_order'       => 0,
			'status'           => 1,
			'layouts'          => array(),
			'stores'           => array(0),
		);

		$customer_subscription + $defaults;

		//Template Data
		$customer_subscription['data_subscriptions'] = array_merge(array(0 => _l(" --- None --- ")), $subscriptions);

		//Render
		output($this->render('customer/subscription/form', $customer_subscription));
	}

	public function view()
	{
		//Page Head
		set_page_info('title', _l("Customer Subscription"));

		//Insert or Update
		$customer_subscription_id = _get('customer_subscription_id');

		$customer_subscription = $this->subscription->getCustomerSubscription($customer_subscription_id);

		if (!$customer_subscription) {
			message("warning", _l("There was no subscription found with the subscription ID: %s", $customer_subscription_id));
			redirect('admin/customer/subscription');
		}

		//Breadcrumbs
		breadcrumb(_l("Home"), site_url('admin'));
		breadcrumb(_l("Customer Subscriptions"), site_url('admin/customer/subscription'));
		breadcrumb(_l("View"), site_url('admin/customer/subscription/view', 'subscription_id=' . $customer_subscription_id));

		//Subscription History
		$history = $this->subscription->getHistory($customer_subscription_id);

		foreach ($history as &$entry) {
			$entry['status_name'] = Subscription::$statuses[$entry['status']];

			if ($entry['transaction_id']) {
				$transaction = $this->transaction->get($entry['transaction_id']);
				html_dump($transaction, 'transaction');
			} elseif ($entry['shipping_id']) {
				$shipping = $this->shipping->get($entry['shipping_id']);
				html_dump($shipping, 'shipping');
			} else {
				$entry['details'] = '';
			}
		}
		unset($entry);

		$customer_subscription['history'] = $history;

		$customer_subscription['status_name'] = Subscription::$statuses[$customer_subscription['status']];

		//Render
		output($this->render('customer/subscription/view', $customer_subscription));
	}

}
