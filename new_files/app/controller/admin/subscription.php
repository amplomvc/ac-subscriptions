<?php

class App_Controller_Admin_Subscription extends Controller
{
	public function index()
	{
		//Page Head
		set_page_info('title', _l("Subscriptions"));

		//Breadcrumbs
		breadcrumb(_l("Home"), site_url('admin'));
		breadcrumb(_l("Subscriptions"), site_url('admin/subscription'));

		//Batch Actions
		$actions = array(
			'enable'  => array(
				'label' => _l("Enable")
			),
			'disable' => array(
				'label' => _l("Disable"),
			),
			'copy'    => array(
				'label' => _l("Copy"),
			),
			'delete'  => array(
				'label' => _l("Delete"),
			),
		);

		$data['batch_action'] = array(
			'actions' => $actions,
			'url'     => site_url('admin/subscription/batch-action'),
		);

		//Listing
		$data['listing'] = $this->listing();

		//Response
		output($this->render('subscription/list', $data));
	}

	public function listing()
	{
		$sort    = (array)_request('sort', array('name' => 'ASC'));
		$filter  = (array)_request('filter');
		$options = array(
			'index'   => 'subscription_id',
			'page'    => _get('page', 1),
			'limit'   => _get('limit', option('admin_list_limit', 20)),
			'columns' => $this->Model_Subscription->getColumns((array)_request('columns')),
		);

		list($subscriptions, $subscription_total) = $this->Model_Subscription->getRecords($sort, $filter, $options, true);

		foreach ($subscriptions as $subscription_id => &$subscription) {
			$subscription['actions'] = array(
				'edit'   => array(
					'text' => _l("Edit"),
					'href' => site_url('admin/subscription/form', 'subscription_id=' . $subscription_id)
				),
				'delete' => array(
					'text' => _l("Delete"),
					'href' => site_url('admin/subscription/delete', 'subscription_id=' . $subscription_id)
				)
			);

			$subscription['plan'] = _l("Every %d %s(s)", $subscription['time'], Subscription::$time_units[$subscription['time_unit']]);

			if ($subscription['shipping_recurring']) {
				$subscription['shipping_plan'] = _l("Every %d %s(s)", $subscription['shipping_time'], Subscription::$time_units[$subscription['shipping_time_unit']]);
			} else {
				$subscription['shipping_plan'] = _l("No Shipping");
			}
		}
		unset($subscription);

		$listing = array(
			'extra_cols'     => $this->Model_Subscription->getColumns(false),
			'records'        => $subscriptions,
			'sort'           => $sort,
			'filter_value'   => $filter,
			'pagination'     => true,
			'total_listings' => $subscription_total,
			'listing_path'   => 'admin/subscription/listing',
			'save_path'      => 'admin/subscription/save',
		);

		$output = block('widget/listing', null, $listing + $options);

		//Response
		if ($this->is_ajax) {
			output($output);
		}

		return $output;
	}

	public function form()
	{
		//Page Head
		set_page_info('title', _l("Subscriptions"));

		//Insert or Update
		$subscription_id = $data['subscription_id'] = _get('subscription_id');

		//Breadcrumbs
		breadcrumb(_l("Home"), site_url('admin'));
		breadcrumb(_l("Subscriptions"), site_url('admin/subscription/'));
		breadcrumb($subscription_id ? _l("Edit") : _l("Add"), site_url('admin/subscription/form', 'subscription_id=' . $subscription_id));

		//Load Information
		$subscription_info = $_POST;

		if (!IS_POST && $subscription_id) {
			$subscription_info = $this->Model_Subscription->getRecord($subscription_id);

			if ($subscribers = $this->Model_Subscription->hasSubscribers($subscription_id)) {
				message('warning', _l("Warning! This Subscription has %s active subscribers, and modifying the data here will change the active subscriptions! It is recommended to <a href=\"%s\">create a new subscription</a> instead.", $subscribers, site_url('admin/subscription/form')));
			}
		}

		//Set Values or Defaults
		$defaults = array(
			'subscription_id'    => $subscription_id,
			'name'               => '',
			'code'               => '',
			'description'        => '',
			'value_function'     => '',
			'recurring'          => 1,
			'cycles'             => 0,
			'time'               => '',
			'time_unit'          => '',
			'day'                => 0,
			'shipping_recurring' => 0,
			'shipping_cycles'    => 0,
			'shipping_time'      => '',
			'shipping_time_unit' => '',
			'shipping_day'       => 0,
			'retries'            => 0,
			'status'             => 1,
		);

		$data += $subscription_info + $defaults;

		//Template Data
		$data['data_days_of_week'] = array(
			'' => _l("-- Select Day of Week --"),
			1  => _l("Monday"),
			2  => _l("Tuesday"),
			3  => _l("Wednesday"),
			4  => _l("Thursday"),
			5  => _l("Friday"),
			6  => _l("Saturday"),
			7  => _l("Sunday"),
		);

		$data['data_days_of_month'] = array('' => _l("-- Select Day of Month --")) + array_slice(range(0, 28), 1, 28, true) + array(31 => _l("End of Month"));

		$data['data_time_units'] = array(
			'D' => _l("Day(s)"),
			'W' => _l("Week(s)"),
			'M' => _l("Month(s)"),
			'Y' => _l("Year(s)"),
		);

		$data['data_statuses'] = array(
			0 => _l("Disabled"),
			1 => _l("Enabled"),
		);

		$data['data_yes_no'] = array(
			1 => _l("Yes"),
			0 => _l("No"),
		);

		//Render
		output($this->render('subscription//form', $data));
	}

	public function save()
	{
		$subscription_id = _request('subscription_id');

		if ($this->Model_Subscription->save($subscription_id, $_POST)) {
			message('success', _l("You have successfully modified Subscriptions!"));
		} else {
			message('error', $this->Model_Subscription->getError());
		}

		if ($this->is_ajax) {
			output_json($this->message->fetch());
		} elseif ($this->message->has('error')) {
			post_redirect('admin/subscription/form', 'subscription_id=' . $subscription_id);
		} else {
			redirect('admin/subscription');
		}
	}

	public function delete()
	{
		if (!empty($_GET['subscription_id']) && $this->validateDelete()) {
			$this->subscription->deleteSubscription($_GET['subscription_id']);

			if (!$this->message->has('error', 'warning')) {
				message('success', _l("You have successfully modified Subscriptions!"));

				redirect('admin/subscription/');
			}
		}

		$this->getList();
	}

	public function batch_update()
	{
		if (!empty($_GET['selected']) && isset($_GET['action'])) {
			foreach ($_GET['selected'] as $subscription_id) {
				switch ($_GET['action']) {
					case 'enable':
						$this->Model_Subscription->save($subscription_id, array('status' => 1));
						break;
					case 'disable':
						$this->Model_Subscription->save($subscription_id, array('status' => 0));
						break;
					case 'delete':
						$this->Model_Subscription->remove($subscription_id);
						break;
					case 'copy':
						$this->Model_Subscription->copy($subscription_id);
						break;
				}

				if ($this->error) {
					break;
				}
			}

			if (!$this->error && !$this->message->has('error', 'warning')) {
				message('success', _l("You have successfully modified Subscriptions!"));

				redirect('admin/subscription/', $this->url->getQueryExclude('action'));
			}
		}

		$this->getList();
	}

	public function generate_code()
	{
		if (!empty($_POST['name'])) {
			$subscription_id = !empty($_POST['subscription_id']) ? $_POST['subscription_id'] : 0;

			output($this->Model_Subscription->generateCode($subscription_id, $_POST['name']));
		}
	}
}
