<? if (!empty($is_checkout)) { ?>
	<div class="message-box warning"><?= _l("Subscriptions are paid separately. You can pay your subscriptions from <a href=\"%s\">your cart</a>.", $url_cart); ?></div>
	<? return; ?>
<? } ?>

<? if (!empty($subscriptions)) { ?>
<form id="subscription-cart" class="cart-info" action="" method="post">
	<table>
		<thead>
			<tr>
				<td class="image">{{Image}}</td>
				<td class="name">{{Subscription Name}}</td>
				<td class="model">{{Subscription Code}}</td>

				<? if (empty($no_price_display)) { ?>
					<td class="total">{{Price}}</td>
				<? } ?>

				<td class="center subscribe">{{Subscribe}}</td>
				<td class="center">{{Remove}}</td>
			</tr>
		</thead>
		<tbody>
			<? foreach ($subscriptions as $subscription) { ?>
				<? $product = $subscription['product']; ?>
				<tr>
					<td class="image">
						<? if ($subscription['thumb']) { ?>
							<a href="<?= $subscription['href']; ?>">
								<img src="<?= $subscription['thumb']; ?>" alt="<?= $product['name']; ?>"
									title="<?= $product['name']; ?>"/>
							</a>
						<? } ?>
					</td>
					<td class="name">
						<a href="<?= $subscription['href']; ?>"><?= $product['name']; ?></a>
						<? if (!$subscription['in_stock']) { ?>
							<span class="out-of-stock"></span>
						<? } ?>

						<? if (!empty($subscription['options'])) { ?>
							<div class="product-option-description">
								<? foreach ($subscription['options'] as $product_option_values) { ?>
									<? foreach ($product_option_values as $product_option_value) { ?>
										<div class="cart-product-option-value">
											<?= _l("%s: %s", $product_option_value['display_name'], $product_option_value['value']); ?>
										</div>
									<? } ?>
								<? } ?>
							</div>
						<? } ?>

						<? if (!empty($product['reward'])) { ?>
							<span class="cart-product-reward"><?= $product['reward']; ?></span>
						<? } ?>
					</td>
					<td class="model"><?= $product['model']; ?></td>

					<? if (!isset($no_price_display)) { ?>
						<td class="total"><?= format('currency', $subscription['total']); ?></td>
					<? } ?>
					<td class="center"><a class="button small subscribe" href="<?= $subscription['url_subscribe']; ?>">{{Subscribe}}</a></td>
					<td class="center">
						<a href="<?= site_url('extension/cart/subscription/remove', 'key=' . $subscription['key']); ?>" data-loading="O" class="button remove">{{X}}</a>
					</td>
				</tr>
			<? } ?>
		</tbody>
	</table>
</form>
<? } ?>

<script type="text/javascript">
	$('#subscription-cart .button.remove').click(function() {
		var $this = $(this);
		$this.loading();
		$.get($this.attr('href'), {}, function(response) {
			$this.loading('stop');
			if (response) {
				$this.closest('tr').remove();
				if (!$('#subscription-cart tbody tr').length) {
					location.reload();
				}
			}
		});
		return false;
	});
</script>
