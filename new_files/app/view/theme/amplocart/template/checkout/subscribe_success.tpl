<?= $is_ajax ? '' : call('header'); ?>

<?= area('left'); ?>
<?= area('right'); ?>

<section id="checkout-success" class="content">
	<header class="row top-row">
		<div class="wrap">
			<?= $is_ajax ? '' : breadcrumbs(); ?>

			<h1>{{Subscription Confirmation}}</h1>
		</div>
	</header>

	<?= area('top'); ?>

	<div class="success-row row">
		<div class="wrap">
			<div class="col xs-12">
				<h2><?= _l("You are now subscribed to the " . $product['name'] . " Box!"); ?></h2>

				<div class="more-info">
						{{You will receive an email receipt for your subscription. Please }}
						<a href="<?= site_url('contact'); ?>">contact us</a>
						{{ if you have any questions.}}
				</div>

				<br />
				<div class="manage">
					{{You can manage your subscription options and preferences from }}<a href="<?= $manage; ?>">{{here.}}</a><br />
					{{Or by vising 'My Account' and choosing the Manage Subscription option.}}
				</div>
			</div>
		</div>
	</div>

	<?= area('bottom'); ?>
</section>

<?= $is_ajax ? '' : call('footer'); ?>
