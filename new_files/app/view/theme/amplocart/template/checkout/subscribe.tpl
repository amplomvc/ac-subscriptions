<?= $is_ajax ? '' : call('header'); ?>
<?= area('left'); ?>
<?= area('right'); ?>

<section id="checkout-subscribe" class="content">
	<header class="row top-row">
		<div class="wrap">
			<?= $is_ajax ? '' : breadcrumbs(); ?>

			<h1>{{Subscription Confirmation}}</h1>
		</div>
	</header>

	<?= area('top'); ?>

	<div class="content-row row">
		<div class="wrap">

			<form action="<?= $subscribe; ?>" method="post" id="subscribe-form">
				<input type="hidden" name="key" value="<?= $cart_key; ?>"/>

				<div class="col xs-12 md-6 box-info">
					<div class="name"><?= $subscription['product']['name']; ?></div>
					<div class="description"><?= html_entity_decode($subscription['product']['description']); ?></div>
					<div class="col xs-12 image">
						<img src="<?= image($subscription['product']['image'], 300, 300); ?>"/>
					</div>
					<div class="options col xs-12 md-8 lg-6">
						<? foreach ($subscription['options'] as $product_option_id => $option) { ?>
							<? foreach ($option as $product_option_value) { ?>
								<div class="option">
									<? if (!empty($product_option_value['display_value'])) { ?>
										<span class="checked value"><?= $product_option_value['name']; ?>: <?= $product_option_value['display_value']; ?></span>
									<? } else { ?>
										<span class="checked value"><?= $product_option_value['value']; ?></span>
									<? } ?>
								</div>
							<? } ?>
						<? } ?>
					</div>
				</div>

				<div class="col xs-12 md-6 payment-info">
					<div id="shipping-address" class="address-select">
						<h2>{{Delivery Address}}</h2>

						<div class="address-list">
							<? $build = array(
								'name'   => 'shipping_address_id',
								'data'   => format_all('address', $data_shipping_addresses),
								'select' => $shipping_address_id,
								'value' => 'address_id',
								'label' => 'formatted',
							); ?>

							<?= build('ac-radio', $build); ?>

							<div class="add-address">
								<a href="<?= site_url('account/address/form'); ?>" class="colorbox">{{Add Address}}</a>
							</div>
						</div>
					</div>

					<div id="payment-address" class="address-select">
						<h2>{{Billing Address}}</h2>

						<div class="address-list">
							<? $build = array(
								'name'   => 'payment_address_id',
								'data'   => format_all('address', $data_payment_addresses),
								'select' => $payment_address_id,
								'value' => 'address_id',
								'label' => 'formatted',
							); ?>

							<?= build('ac-radio', $build); ?>

							<div class="add-address">
								<a href="<?= site_url('account/address/form'); ?>" class="colorbox">{{Add Address}}</a>
							</div>
						</div>
					</div>

					<div class="card_select clear">
						<h2>{{Payment Method}}</h2>
						<?= block('account/card/select'); ?>
					</div>
				</div>

				<div class="col xs-12 subscription-submit">
					<noscript>
						<style>.form-item.submit {
								display: none
							}</style>
						<div class="no_javascript">{{You must enable javascript in your browser in order to continue}}</div>
					</noscript>

					<div class="form-item submit">
						<div class="price"><?= !empty($subscription['total_with_savings']) ? $subscription['total_with_savings'] : $subscription['total_display']; ?></div>
						<button data-loading="{{Subscribing...}}">{{Subscribe}}</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</section>

<script type="text/javascript">
	$('#subscribe-form').submit(function () {
		$(this).find('.submit button').loading();
	});
</script>

<?= $is_ajax ? '' : call('footer'); ?>
