<? $sprite = theme_sprite('sprite.png'); ?>

<div class="account-info account-subscription">
	<h1>{{My Subscriptions}}</h1>

	<div class="subscription-list">
		<? if (!empty($subscriptions)) { ?>
			<? foreach ($subscriptions as $subscription_id => $subscription) { ?>
				<div class="section subscription col xs-12 left no-whitespace-hack">
					<div class="heading col xs-12">
						<h3 class="name"><?= $subscription['product']['name']; ?></h3>

						<div class="status"><?= Subscription::$statuses[$subscription['status']]; ?></div>
					</div>

					<div class="subscription-details col xs-12 left no-whitespace-hack">
						<div class="col xs-12 md-4 lg-5 image left top">
							<img <?= img($subscription['product']['image'], 294, 205); ?> />
						</div>

						<div class="col xs-12 md-8 lg-7 details left top">
							<form action="<?= site_url('account/subscription/save', 'subscription_id=' . $subscription_id); ?>" class="form-editor subscription-options read section left col xs-12" method="post">
								<div class="info col xs-7 left top">
									<h3 class="price"><?= $subscription['total_display']; ?></h3>
								</div>

								<div class="buttons col xs-5 right top">
									<a class="reading edit-form">{{Edit}}</a>
									<a class="editing cancel-form">{{Cancel}}</a>
								</div>

								<? if (!empty($subscription['options'])) { ?>
									<div class="section options">
										<? foreach ($subscription['options'] as $option) { ?>
											<? $option = current($option); ?>
											<div class="option">
												<div class="name col xs-8 left"><?= $option['display_name']; ?></div>
												<div class="value reading col xs-4 middle right" data-name="<?= $option['name']; ?>"><?= $option['value']; ?></div>
												<div class="value editing col xs-4 middle right">
													<div class="quantity incrementor">
														<a class="sprite minus"></a>
														<input type="text" readonly class="<?= $option['name']; ?>" name="<?= $option['name']; ?>" value="<?= $option['value']; ?>" data-values="<?= implode(',', $options[$option['name']]); ?>"/>
														<a class="sprite plus"></a>
													</div>
												</div>
											</div>
										<? } ?>
									</div>
								<? } ?>

								<div class="section buttons save-form submit-options-form editing col no-whitespace-hack xs-12">
									<div class="save-btn col xs-12 md-8 left">
										<button data-loading="{{Saving...}}">{{Save Changes}}</button>
									</div>

									<div class="more-actions col xs-12 md-4 right">
										<? if ($subscription['status'] === Subscription::STATUS_PAUSED) { ?>
											<a href="<?= site_url('account/subscription/resume', 'subscription_id=' . $subscription_id); ?>">{{Resume}}</a>
										<? } else { ?>
											<a href="<?= site_url('account/subscription/pause-form', 'subscription_id=' . $subscription_id); ?>">{{Freeze}}</a>
											<a href="<?= site_url('account/subscription/cancel-form', 'subscription_id=' . $subscription_id); ?>">{{Remove}}</a>
										<? } ?>
									</div>
								</div>
							</form>
						</div>

						<div class="col xs-12 left top">
							<form action="<?= site_url('account/subscription/save', 'subscription_id=' . $subscription_id); ?>" class="form-editor read section left col xs-12 shipping-address-form address" method="post" data-reload="true">
								<div class="label col xs-7 left top">
									<h3>{{Shipping Address}}</h3>
								</div>

								<div class="buttons col xs-5 right top">
									<a class="reading edit-form">{{Change}}</a>
									<a class="editing cancel-form">{{Cancel}}</a>
								</div>

								<div class="shipping-address address col xs-12 left no-whitespace-hack">
									<div class="formatted reading">
										<? if ($address_format = format('address', $subscription['shipping_address_id'])) { ?>
											<?= $address_format; ?>
										<? } else { ?>
											{{Please choose a delivery address}}
										<? } ?>
									</div>

									<div class="editing address-change">
										<?
										$address_options = array(
											'index'      => 'shipping_address',
											'address_id' => $subscription['shipping_address_id']
										);
										?>

										<?= call('smoothiesy/address', array($address_options), true); ?>
									</div>
								</div>

								<div class="buttons editing col xs-12 submit">
									<button class="save-form">{{Change Address}}</button>
								</div>
							</form>

							<form action="<?= site_url('account/subscription/save', 'subscription_id=' . $subscription_id); ?>" class="form-editor read section left col xs-12 payment-profile-form address" method="post" data-noajax="true">
								<div class="label col xs-7 left top">
									<h3>{{Payment Method}}</h3>
								</div>

								<div class="buttons col xs-5 right top">
									<a class="reading edit-form">{{Change}}</a>
									<a class="editing cancel-form">{{Cancel}}</a>
								</div>

								<div class="payment-profile formatted reading">
									<? if ($payment_format = format('payment', $subscription['payment_profile_id'])) { ?>
										<?= $payment_format; ?>
									<? } else { ?>
										{{Please add a payment profile for this subscription}}
									<? } ?>
								</div>

								<div class="payment-profile box editing">
									<?= call('smoothiesy/payment', array('profile_id' => $subscription['payment_profile_id']), true); ?>
								</div>

								<div class="buttons editing col xs-12 submit">
									<button class="save-form">{{Change Method}}</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			<? } ?>
		<? } else { ?>
			{{You have not ordered any subscriptions.}} <a href="<?= site_url(); ?>">{{Order a subscription today!}}</a>
		<? } ?>
	</div>
</div>
</div>

<script type="text/javascript">
	$('.form-editor').form_editor();

	(function ($) {
		var price_set = <?= json_encode(Smoothiesy::$price_set); ?>;

		$('.subscription-options input').change(update_price);

		function update_price() {
			var $form = $(this).closest('form');

			var people = parseInt($form.find('[name=people]').val()),
				weekly = parseInt($form.find('[name=weekly]').val()),
				protein = parseInt($form.find('[name=protein]').val()),
				superfood = parseInt($form.find('[name=superfood]').val());

			var extras = protein + superfood;

			var total_each = price_set[people][weekly];
			var total = (total_each + Math.max(0, extras - people)) * people * weekly;

			$form.find('.price').html('$' + total.toFixed(2) + ' / week');
		}
	})($);
</script>
