<?= $is_ajax ? '' : call('header'); ?>
<?= area('left'); ?>
<?= area('right'); ?>

<section class="pause-subscription content">
	<header class="row top-row">
		<div class="wrap">
			<?= $is_ajax ? '' : breadcrumbs(); ?>

			<h1>{{Pause Subscription}}</h1>
		</div>
	</header>

	<?= area('top'); ?>

	<div class="content-row row">
		<div class="wrap">
			<form action="<?= site_url('account/subscription/pause', 'subscription_id=' . $customer_subscription_id); ?>" method="post" class="subscription-pause-form">
				<div class="subscription">
					<h2><?= $subscription['product']['name']; ?></h2>

					<h4>{{How many Billing Cycles would you like to pause your subscription for?}}</h4>

					<div class="help">{{You will not be billed during this period}}</div>
				</div>

				<div class="pause-cycles clear buttons">
					<?= build('radio', array(
						'name'   => 'pause_cycles',
						'data'   => $data_pause_cycles,
						'select' => $pause_cycles,
						'#class' => 'panel pill',
					)); ?>
				</div>

				<div class="col xs-12 buttons">
					<div class="back col xs-12 sm-6 lg-3">
						<a class="button" href="<?= site_url('account/subscription', 'subscription_id=' . $subscription['customer_subscription_id']); ?>">{{Nevermind}}</a>
					</div>
					<div class="confirm col xs-12 sm-6 lg-3">
						<button>{{Pause}}</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<?= area('bottom'); ?>

</section>

<?= $is_ajax ? '' : call('footer'); ?>
