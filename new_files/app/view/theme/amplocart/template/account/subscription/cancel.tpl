<?= $is_ajax ? '' : call('header'); ?>
<?= area('left'); ?>
<?= area('right'); ?>

<section class="subscription-update subscription-cancel content">
	<header class="row top-row">
		<div class="wrap">
			<?= $is_ajax ? '' : breadcrumbs(); ?>

			<h1>{{Subscription Cancellation}}</h1>
		</div>
	</header>

	<?= area('top'); ?>

	<div class="content-row row">
		<div class="wrap">
			<form action="<?= site_url('account/subscription/cancel', 'subscription_id=' . $customer_subscription_id); ?>" method="post" class="subscription-cancel-form">
				<div class="subscription">
					<h2><?= $subscription['product']['name']; ?></h2>
					<h4>{{Are you sure you want to cancel your subscription?}}</h4>
				</div>

				<div class="col xs-12 buttons">
					<div class="back col xs-12 sm-6 lg-3">
						<a class="button" href="<?= site_url('account/subscription', 'subscription_id=' . $subscription['customer_subscription_id']); ?>">{{No Don't Cancel!}}</a>
					</div>
					<div class="confirm col xs-12 sm-6 lg-3">
						<button>{{Confirm Cancellation}}</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<?= area('bottom'); ?>

</section>

<?= $is_ajax ? '' : call('footer'); ?>
