<?= $is_ajax ? '' : call('header'); ?>

<?= $is_ajax ? '' : breadcrumbs(); ?>

<form id="subscription_view" action="<?= site_url('admin/customer/subscription/save', 'subscription_id=' . $customer_subscription_id); ?>" method="post" enctype="multipart/form-data" class="box">

	<div class="heading">
		<h1><img src="<?= theme_url('image/category.png'); ?>" alt=""/> {{Customer Subscription}}</h1>

		<div class="buttons">
			<input type="submit" class="button" value="{{Save}}"/>
			<a href="<?= site_url('admin/customer/subscription'); ?>" class="button">{{Cancel}}</a>
		</div>
	</div>

	<section>
		<div id="tabs" class="htabs">
			<a href="#tab-overview">{{Overview}}</a>
			<a href="#tab-history">{{History}}</a>
		</div>

		<div id="tab-overview" class="clearfix">
			<div class="left image">
				<img <?= img($product['image'], 400); ?> />
			</div>
			<div class="left info">
				<div class="product_name"><?= $product['name']; ?></div>
				<div class="description"><?= $product['description']; ?></div>
			</div>
			<div class="right status <?= slug($status_name); ?>"><?= $status_name; ?></div>
			<div class="clear options">
				<? foreach ($options as $values) { ?>
					<? foreach ($values as $value) { ?>
						<div class="option">
							<? if (!empty($value['display_value'])) { ?>
								<span class="name"><?= $value['display_name']; ?></span> <span class="value"><?= $value['display_value']; ?></span>
							<? } else { ?>
								<span class="name"><?= $value['display_name']; ?></span> <span class="value"><?= $value['value']; ?></span>
							<? } ?>
						</div>
					<? } ?>
				<? } ?>
			</div>

			<div class="right total">
				<span class="price"><?= $total_display; ?></span>
			</div>
		</div>
		<!-- /tab-overview -->

		<div id="tab-history">
			<table id="history_list" class="list">
				<thead>
					<tr>
						<td>{{Date}}</td>
						<td>{{Transaction Type}}</td>
						<td>{{Comment}}</td>
						<td>{{Details}}</td>
						<td>{{Status}}</td>
					</tr>
				</thead>
				<tbody>
					<? foreach ($history as $entry) { ?>
						<tr class="<?= slug($entry['status_name']); ?>">
							<td><?= $entry['date_added']; ?></td>
							<td><?= $entry['type']; ?></td>
							<td><?= $entry['comment']; ?></td>
							<td><?= $entry['details']; ?></td>
							<td><?= $entry['status_name']; ?></td>
						</tr>
					<? } ?>
				</tbody>
			</table>
		</div>
		<!-- /tab-history -->

	</section>

</form>

<script type="text/javascript">
	$('#tabs a').tabs();
</script>



<?= $is_ajax ? '' : call('footer'); ?>
