<?= $is_ajax ? '' : call('admin/header'); ?>

<div class="section">
	<?= $is_ajax ? '' : breadcrumbs(); ?>
	<div class="box">
		<div class="heading">
			<h1><img src="<?= theme_url('image/setting.png'); ?>" alt=""/> {{Customer Subscriptions}}</h1>

			<? if (!empty($batch_action) && user_can('w', 'admin/customer/subscription/batch_action')) { ?>
				<div class="batch_actions">
					<?= block('widget/batch_action', null, $batch_action); ?>
				</div>
			<? } ?>

			<? if (user_can('w', 'admin/customer/subscription/form')) { ?>
				<div class="buttons">
					<a href="<?= site_url('admin/customer/subscription/form'); ?>" class="button">{{Create Customer Subscription}}</a>
				</div>
			<? } ?>
		</div>
		<div class="section">
			<div class="section">
				<?= block('widget/views', null, array(
					'group'           => 'customer_subscriptions',
					'view_listing_id' => $this->Model_Customer_Subscription->getViewListingId(),
				)); ?>
			</div>
		</div>
	</div>
</div>

<?= $is_ajax ? '' : call('admin/footer'); ?>
