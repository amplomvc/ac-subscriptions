<?= $is_ajax ? '' : call('header'); ?>
<div class="section">
	<?= $is_ajax ? '' : breadcrumbs(); ?>
	<div class="box">
		<div class="heading">
			<h1>
				<img src="<?= theme_url('image/category.png'); ?>" alt=""/> <?= $head_title; ?></h1>

			<div class="buttons">
				<a onclick="$('#form').submit();" class="button">{{Save}}</a>
				<a href="<?= $cancel; ?>" class="button">{{Cancel}}</a>
			</div>
		</div>
		<div class="section">
			<div id="tabs" class="htabs">
				<a href="#tab-general"><?= $tab_general; ?></a>
				<a href="#tab-data"><?= $tab_data; ?></a>
				<a href="#tab-design"><?= $tab_design; ?></a>
			</div>

			<form action="<?= $action; ?>" method="post" enctype="multipart/form-data" id="form">
				<div id="tab-general">
					<table class="form">
						<tr>
							<td class="required"> {{Name}}</td>
							<td><input type="text" name="name" size="60" value="<?= $name; ?>"/></td>
						</tr>
						<tr>
							<td>{{Meta Keyword}}</td>
							<td>
								<textarea name="meta_keywords" rows="4" cols="60"><?= $meta_keywords; ?></textarea>
							</td>
						</tr>
						<tr>
							<td>{{Meta Description}}</td>
							<td>
								<textarea name="meta_description" rows="8" cols="60"><?= $meta_description; ?></textarea>
							</td>
						</tr>
						<tr>
							<td>{{Description}}</td>
							<td>
								<textarea class="ckedit" name="description"><?= $description; ?></textarea>
							</td>
						</tr>
					</table>
				</div>
				<div id="tab-data">
					<table class="form">
						<tr>
							<td>{{Parent}}</td>
							<td>
								<?=
								build(array(
									'type' => 'select',
									'name'   => 'parent_id',
									'data'   => $data_categories,
									'select' => (int)$parent_id,
									'value' => 'category_id',
									'label' => 'pathname',
								)); ?>
							</td>
						</tr>
						<tr>
							<td>{{Store}}</td>
							<td>
								<?=
								build(array(
									'type' => 'multiselect',
									'name'   => "category_store",
									'data'   => $data_stores,
									'select' => $stores,
									'value' => 'store_id',
									'label' => 'name',
								)); ?>
							</td>
						</tr>
						<tr>
							<td>{{Alias}}</td>
							<td>
								<input type="text" onfocus="$(this).next().display_error('<?= $warning_generate_url; ?>', 'gen_url');" name="alias" value="<?= $alias; ?>"/>
								<a class="gen_url" onclick="generate_url($(this))">{{Generate Url}}</a>
							</td>
						</tr>
						<tr>
							<td>{{Image}}</td>
							<td>
								<?= $this->builder->imageInput("image", $image); ?>
							</td>
						</tr>
						<tr>
							<td>{{Sort Order}}</td>
							<td><input type="text" name="sort_order" value="<?= $sort_order; ?>" size="1"/></td>
						</tr>
						<tr>
							<td>{{Status}}</td>
							<td><?=
								build(array(
									'type' => 'select',
									'name'   => 'status',
									'data'   => $statuses,
									'select' => $status
								)); ?></td>
						</tr>
					</table>
				</div>
				<div id="tab-design">
					<table class="list">
						<thead>
						<tr>
							<td class="left">{{Store}}</td>
							<td class="left">{{Layout}}</td>
						</tr>
						</thead>
						<tbody>
						<? foreach ($data_stores as $store) { ?>
							<tr>
								<td class="left"><?= $store['name']; ?></td>
								<td class="left">
									<?=
									build(array(
										'type' => 'select',
										'name'   => "layouts[$store[store_id]][layout_id]",
										'data'   => $data_layouts,
										'select' => isset($layouts[$store['store_id']]) ? (int)$layouts[$store['store_id']] : '',
										'value' => 'layout_id',
										'label' => 'name'
									)); ?>
								</td>
							</tr>
						<? } ?>
						</tbody>
					</table>
				</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
	function generate_url(context) {
		$.clear_errors('gen_url');

		name = $('input[name=name]').val();

		if (!name) {
			alert("Please make a name for this Category before generating the URL");
			return;
		}

		data = {category_id: <?= (int)$category_id; ?>, name: name};

		$(context).fade_post("<?= $url_generate_url; ?>", data, function (response) {
			$('input[name="alias"]').val(response);
		});
	}

	//Tabs
	$('#tabs a').tabs();
</script>

<?= $is_ajax ? '' : call('footer'); ?>
