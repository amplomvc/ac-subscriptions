<?= $is_ajax ? '' : call('admin/header'); ?>

<div class="section">
	<?= $is_ajax ? '' : breadcrumbs(); ?>

	<form action="<?= site_url('admin/subscription/save', 'subscription_id=' . $subscription_id); ?>" method="post" enctype="multipart/form-data" class="box ctrl-save">
		<div class="heading">
			<h1><img src="<?= theme_url('image/category.png'); ?>" alt=""/> {{Subscriptions}}</h1>

			<div class="buttons">
				<button>{{Save}}</button>
				<a href="<?= site_url('admin/subscription/'); ?>" class="button cancel">{{Cancel}}</a>
			</div>
		</div>

		<div class="section">
			<table class="form">
				<tr>
					<td class="required"> {{Subscription Title}}</td>
					<td><input type="text" name="name" size="60" value="<?= $name; ?>"/></td>
				</tr>
				<tr>
					<td class="required">{{Code}}</td>
					<td>
						<input type="text" onfocus="$(this).next().display_error('{{Warning: must be unique!}}', 'gen_code');" name="code" value="<?= $code; ?>"/>
						<a class="gen_url" onclick="generate_code($(this))">{{[Generate Code]}}</a>
					</td>
				</tr>
				<tr>
					<td>{{Description}}</td>
					<td><textarea class="ckedit" name="description"><?= $description; ?></textarea></td>
				</tr>
				<tr>
					<td class="required"> <?= _l("Value Modifier Function:<span class=\"help\">This will modify the total subscription product value. For Trial / Discounted Periods, etc..</span>"); ?></td>
					<td>
						<input type="text" name="value_function" size="60" value="<?= $value_function; ?>"/>
						<span class="help">{{Use %d for the total product value.<br/>(eg: for trial period '0', for 50% off '.5*%d', for 6 installments '%d/6' and set cycles to 6, etc.)}}</span>
					</td>
				</tr>
				<tr>
					<td class="required"> {{Recurring Billing?}}</td>
					<td><?= build(array(
							'type'   => 'radio',
							'name'   => 'recurring',
							'data'   => $data_yes_no,
							'select' => $recurring
						)); ?>
						<table id="recur_bill_info" class="form">
							<tr>
								<td class="required">
									<div>{{Number of Cycles:}}</div>
									<span class="help">{{Leave as 0 for unlimited (until Cancelled)}}</span>
								</td>
								<td><input type="text" name="cycles" size="3" value="<?= $cycles; ?>"/></td>
							</tr>
							<tr>
								<td class="required"> {{Time Period}}</td>
								<td><input type="text" name="time" size="3" value="<?= $time; ?>"/></td>
							</tr>
							<tr>
								<td class="required"> {{Unit of Time Period}}</td>
								<td><?= build(array(
										'type'   => 'select',
										'name'   => 'time_unit',
										'data'   => $data_time_units,
										'select' => $time_unit
									)); ?></td>
							</tr>
							<tr>
								<td>{{Bill On:}}<span class="help">{{Leave blank to start billing immediately}}</span></td>
								<td id="day_select">
									<div id="day_of_week"><?= build(array(
											'type'   => 'select',
											'name'   => 'day',
											'data'   => $data_days_of_week,
											'select' => $day
										)); ?></div>
									<div id="day_of_month"><?= build(array(
											'type'   => 'select',
											'name'   => 'day',
											'data'   => $data_days_of_month,
											'select' => $day
										)); ?></div>
									<div id="day_of_year">
										<input type="text" size="3" maxlength="3" name="day" value="<?= $day; ?>"/>
										<span class="help">{{Must be between 1 and 365.}}</span>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>

				<tr>
					<td>
						{{Failed Payment Retry Attempts:}}
						<span class="help">{{If payment has failed, system will continue trying the payment this many times. If it still fails, subscription will be cancelled.}}</span>
					</td>
					<td><input type="text" name="retries" size="2" value="<?= $retries; ?>"/></td>
				</tr>
				<tr>
					<td class="required"> {{Recurring Shipping?}}</td>
					<td><?= build(array(
							'type'   => 'radio',
							'name'   => 'shipping_recurring',
							'data'   => $data_yes_no,
							'select' => $shipping_recurring
						)); ?>
						<table id="recur_ship_info" class="form">
							<tr>
								<td class="required"> {{Cycles}}</td>
								<td><input type="text" name="shipping_cycles" size="3" value="<?= $shipping_cycles; ?>"/>
								</td>
							</tr>
							<tr>
								<td class="required"> {{Shipping Time}}</td>
								<td><input type="text" name="shipping_time" size="3" value="<?= $shipping_time; ?>"/></td>
							</tr>
							<tr>
								<td class="required"> {{Shipping Time Unit}}</td>
								<td><?= build(array(
										'type'   => 'select',
										'name'   => 'shipping_time_unit',
										'data'   => $data_time_units,
										'select' => $shipping_time_unit
									)); ?></td>
							</tr>
							<tr>
								<td>{{Ship On:}}</td>
								<td id="ship_day_select">
									<div id="ship_day_of_week"><?= build(array(
											'type'   => 'select',
											'name'   => 'shipping_day',
											'data'   => $data_days_of_week,
											'select' => $shipping_day
										)); ?></div>
									<div id="ship_day_of_month"><?= build(array(
											'type'   => 'select',
											'name'   => 'shipping_day',
											'data'   => $data_days_of_month,
											'select' => $shipping_day
										)); ?></div>
									<div id="ship_day_of_year">
										<input type="text" size="3" maxlength="3" name="shipping_day" value="<?= $shipping_day; ?>"/>
										<span class="help">{{Must be between 1 and 365.}}</span>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>{{Status}}</td>
					<td><?= build(array(
							'type'   => 'select',
							'name'   => 'status',
							'data'   => $data_statuses,
							'select' => $status
						)); ?></td>
				</tr>
			</table>
		</div>
	</form>
</div>

<script type="text/javascript">
	var day_of_week = $('#day_of_week').remove();
	var day_of_month = $('#day_of_month').remove();
	var day_of_year = $('#day_of_year').remove();

	$('[name=time_unit]').change(function () {
		var view = $('#day_select');

		switch ($(this).val()) {
			case 'D':
				view.html('');
				break;
			case 'W':
				view.html(day_of_week);
				break;
			case 'M':
				view.html(day_of_month);
				break;
			case 'Y':
				view.html(day_of_year);
				break;
		}
	}).change();

	$('[name=recurring]').change(function () {
		if ($(this).is(":checked")) $('#recur_bill_info').toggle($(this).is('[value=1]'));
	}).change();

	var ship_day_of_week = $('#ship_day_of_week').remove();
	var ship_day_of_month = $('#ship_day_of_month').remove();
	var ship_day_of_year = $('#ship_day_of_year').remove();

	$('[name=shipping_time_unit]').change(function () {
		var view = $('#ship_day_select');

		switch ($(this).val()) {
			case 'D':
				view.html('');
				break;
			case 'W':
				view.html(ship_day_of_week);
				break;
			case 'M':
				view.html(ship_day_of_month);
				break;
			case 'Y':
				view.html(ship_day_of_year);
				break;
		}
	}).change();

	$('[name=shipping_recurring]').change(function () {
		if ($(this).is(":checked")) $('#recur_ship_info').toggle($(this).is('[value=1]'));
	}).change();

	function generate_code(context) {
		name = $('input[name=name]').val();

		if (!name) {
			alert("Please make a Title for this subscription before generating the Code");
		} else {
			data = {subscription_id:<?= (int)$subscription_id; ?>, name: name};
			$(context).fade_post("<?= site_url('admin/subscription/generate_code'); ?>", data, function (json) {
				$('input[name=code]').val(json);
			});
		}
	}

	$('[name=recurring]').change(function () {
		cycles_tr = $('[name=cycles]').closest('tr');

		if ($(this).val() === '1') {
			cycles_tr.slideDown();
		} else {
			cycles_tr.fadeOut(0);
		}
	});

	$('[name=recurring]:checked').change();
</script>

<?= $is_ajax ? '' : call('admin/footer'); ?>
