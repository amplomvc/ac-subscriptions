<?php
/**
 * Algorithm: Ganon
 */

$node('[href="#tab-special"]')->after(<<<HTML
<a href="#tab-subscription">{{Subscription}}</a>
HTML
);

$node('#tab-special')->after(<<<HTML
				<div id="tab-subscription">
					<table class="form">
						<tr>
							<td>
								{{Subscriptions}}
								<?= build(array(
									'type' => 'select',
									'name'  => 'subscription',
									'data'  => \$data_subscriptions,
									'value'   => 'subscription_id',
									'label' => 'name',
								)); ?>
								<br /><br />
								<a class="button" id="add_subscription">{{Add Subscription}}</a>
							</td>
							<td>
								<ul id="subscription_list" class="easy_list">
									<? foreach (\$product_subscriptions as \$row => \$subscription) { ?>
										<li class="subscription" data-row="<?= \$row; ?>">
											<input class="subscription_id" type="hidden" name="product_subscriptions[<?= \$row; ?>][subscription_id]" value="<?= \$subscription['subscription_id']; ?>" />
											<span class="name"><?= \$subscription['name']; ?></span>
											<input class="sort_order" type="text" name="product_subscriptions[<?= \$row; ?>][sort_order]" value="<?= \$subscription['sort_order']; ?>" />
											<? if (empty(\$subscription['no_delete'])) { ?>
												<a class="delete button text" onclick="\$(this).closest('li').remove()">{{Delete}}</a>
											<? } ?>
										</li>
									<? } ?>
								</ul>
							</td>
						</tr>
					</table>
				</div> <!-- /tab-subscription -->

<script type="text/javascript">
\$('#subscription_list').ac_template('subscription_list', {defaults: <?= json_encode(\$product_subscriptions['__ac_template__']); ?>});

\$('#add_subscription').click(function(){
	select = \$('select[name=subscription]');

	sub_row = \$.ac_template('subscription_list', 'add', {subscription_id: select.val()} );

	sub_row.find('.name').html(select.find('option[value="+select.val()+"]').html());

	\$('#subscription_list').update_index('.sort_order');
});

\$('#subscription_list').sortable({cursor:'move', stop: function(){\$(this).update_index('.sort_order');} });
</script>
HTML
);
